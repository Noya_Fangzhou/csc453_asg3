# require("./30114609_enum/P2Enumerable.rb")
# require("./30114609_tree/P2Tree.rb")
require("./txie3_enum.rb")
require("./txie3_tree.rb")
=begin
	
The structure of the binary tree used in this test is 

										-------
										|  0  |
										-------
							-------        |    	-------
							|  1  |	---------------	|  2  |
							-------				    -------

=end
class P2Tree
	include P2Enumerable
	# alias p2each p2each_tree
end
class Array
	alias p2each each
	include P2Enumerable
end

# tree1 = P2Tree.new(0)
# tree1.left = P2Tree.new(1)
# tree1.right = P2Tree.new(2)
lc1 = P2Tree.new(1,2,nil,nil)
rc1 = P2Tree.new(2,2,nil,nil)
tree1 = P2Tree.new(0,1,lc1,rc1)



# used to print the detail of the test case

def printerror(methodName, expected, actual, classname)
	raise "#{methodName} for #{classname} error\nexpected #{expected} actual is #{actual}" if actual != expected
	puts "#{methodName} for #{classname} passed"
end

def test_tree_p2all?(root,test_case)
	p "#{__method__}"
	res =root.p2all? {|n| n.val <1}
	printerror(__method__,false,res,test_case)
	
end

# test_tree_p2all?(tree2,"empty tree")
test_tree_p2all?(tree1,"tree")

def test_tree_p2any?(root,test_case)
	p "#{__method__}"
	res =root.p2any? {|n| n.val <1}
	printerror(__method__,true,res,test_case)
end

test_tree_p2any?(tree1,"tree")

def test_tree_p2collect(root,test_case)
	p "#{__method__}"
	res = root.p2collect {|n| n.val ** 2}
	printerror(__method__,[0,1,4],res,test_case)
end

test_tree_p2collect(tree1,"tree")

def test_tree_p2collect_concat(root,test_case)
	p "#{__method__}"
	res = root.p2collect_concat {|n| [n,n.val ** 2]}
	printerror(__method__,[root,0,root.left,1,root.right,4],res,test_case)
end

test_tree_p2collect_concat(tree1,"tree")

def test_tree_p2count(root,test_case)
	p "#{__method__}"
	res = root.p2count
	printerror(__method__, 3, res, test_case)
end

test_tree_p2count(tree1,"tree")

def test_tree_p2cycle(root,test_case)
	p "#{__method__}"
	res = []
	res = root.p2cycle(2) {|n| res<<n.val}
	puts res
	printerror(__method__, [0,1,3,4,2,5,0,1,3,4,2,5], res, test_case)
end

# test_tree_p2cycle(tree1,"tree")

def test_tree_p2detect(root,test_case)
	p "#{__method__}"
	res = root.p2detect(nil) {|n| n.val%2 == 0}
	printerror(__method__, root, res, test_case)
end

test_tree_p2detect(tree1,"tree")

def test_tree_p2drop(root,test_case)
	p "#{__method__}"
	res =root.p2drop(1)
	printerror(__method__,[root.left,root.right],res,test_case)
end

test_tree_p2drop(tree1,"tree")

def test_tree_p2drop_while(root,test_case)
	p "#{__method__}"
	res = root.p2drop_while {|e| e.val >= 3}
	printerror(__method__,[root,root.left,root.right],res,test_case)

end

test_tree_p2drop_while(tree1,"tree")

def test_tree_p2each_cons(root,test_case)
	p "#{__method__}"
	root.p2each_cons(2) {|n| puts n}
	# printerror(__method__,12,sum,"array")
end

test_tree_p2each_cons(tree1,"tree")

def test_tree_p2each_slice(root,test_case)
	p "#{__method__}"
	temp = Array.new
	root.p2each_slice(2) {|e| puts e}#{|e| temp << e.all? {|i| i.val<3}}
	printerror(__method__, true, temp.all?{|i| i == true}, test_case)
end

test_tree_p2each_slice(tree1,"tree")

def test_tree_p2each_with_index(root,test_case)
	p "#{__method__}"
	res = Array.new
	root.p2each_with_index(3) {|e, index| res << e.val - index  }
	printerror(__method__, [0,0,0],res,test_case)

end

test_tree_p2each_with_index(tree1,"tree")

def test_tree_p2entries(root,test_case)
	p "#{__method__}"
	res = root.p2entries
	printerror(__method__,[root,root.left,root.right],res, test_case)
	
end

test_tree_p2entries(tree1,"tree")

def test_tree_p2find(root,test_case)
	p "#{__method__}"
	res =root.p2find(nil) {|e| e.val%2 == 1}
	printerror(__method__,root.left,res,test_case)
end

test_tree_p2find(tree1,"tree")

def test_tree_p2find_all(root,test_case)
	p "#{__method__}" 
	res = root.p2find_all {|e| e.val<=1}
	printerror(__method__,[root,root.left],res,test_case)
end

test_tree_p2find_all(tree1,"tree")

def test_tree_p2find_index(root,test_case)
	p "#{__method__}"
	res = root.p2find_index {|e| e.val<=2}
	printerror(__method__,0,res,test_case)
end

test_tree_p2find_index(tree1,"tree")

def test_tree_p2first(root,test_case)
	p "#{__method__}"
	res = root.p2first(1) {|e| e<3}
	printerror(__method__,[root],res,test_case)
end

test_tree_p2first(tree1,"tree")

def test_tree_p2group_by(root,test_case)
	p "#{__method__}"
	res = root.p2group_by {|e| e.val%2}
	printerror(__method__,{0=>[root,root.right],1=>[root.left]},res,test_case)
end

test_tree_p2group_by(tree1,"tree")

def test_tree_p2inject(root,test_case)
	p "#{__method__}"
	res = root.p2inject(0) {|e, res| res * e.val }
	printerror(__method__,0,res,test_case)
end

test_tree_p2inject(tree1,"tree")

def test_tree_p2para0inject(root,test_case) 
	p "#{__method__}"
	res = root.p2para0inject {|res, e| res.val * e.val  }
	printerror(__method__,0,res,test_case)
	
end

# test_tree_p2para0inject(tree1,"tree")

def test_tree_p2minmax(root,test_case)
	p "#{__method__}"
	res = root.p2minmax {|e1,e2| -(e1.val) <=> -(e2.val)}
	printerror(__method__,[root.right,root],res,test_case)
	
end

test_tree_p2minmax(tree1,"tree")

def test_tree_p2minmax_by(root,test_case)
	p "#{__method__}"
	res = root.p2minmax_by {|e| (e.val)**2}
	printerror(__method__,[root,root.right],res,test_case)
end

test_tree_p2minmax_by(tree1,"tree")

def test_tree_p2partition(root,test_case)
	p "#{__method__}" 
	res = root.p2partition {|e| e.val <= 1}
	printerror(__method__,[[root,root.left],[root.right]],res,test_case)
end

test_tree_p2partition(tree1,"tree")

def test_tree_p2reject(root,test_case)
	p "#{__method__}"
	res = root.p2reject {|e| (e.val)%2==1}
	printerror(__method__,[root,root.right],res,test_case)
end

test_tree_p2reject(tree1,"tree")

def test_tree_p2take(root,test_case)
	p "#{__method__}"
	res = root.p2take(2)
	printerror(__method__,[root,root.left],res,test_case)
end

test_tree_p2take(tree1,"tree")

def test_tree_p2take_while(root,test_case) 
	p "#{__method__}"
	res = root.p2take_while {|e| e.val != 1}
	printerror(__method__,[root],res,test_case)
end

test_tree_p2take_while(tree1,"tree")

def test_tree_p2to_a(root,test_case)
	p "#{__method__}"
	res = root.p2to_a
	printerror(__method__,[root,root.left,root.right],res,test_case)

end

test_tree_p2to_a(tree1,"tree")

def test_tree_p2to_h(root,test_case)
	p "#{__method__}"
	res = Hash.new
	root.p2each_tree_level {|n,l| res<<{n=>l}}
	printerror(__method__,{root=>1,root.left=>2,root.right=>2},res,test_case)
	# puts %i[hello world].each_with_index.to_h
end

test_tree_p2to_h(tree1,"tree")





def test_tree_p2min(root,test_case)
	p "#{__method__}"
	res = root.p2min {|e1,e2| -e1.val <=> -e2.val}
	printerror(__method__,root.right,res,test_case)
end

# test_tree_p2min(tree1,"tree")

def test_tree_p2min_by(root,test_case)
	p "#{__method__}"
	res = root.p2min_by {|e| (e.val)**2 }
	printerror(__method__,root,res,test_case)
end

# test_tree_p2min_by(tree1,"tree")

def test_tree_p2max(root,test_case)
	p "#{__method__}"
	res = root.p2max {|e1,e2| -e1.val <=> -e2.val}
	printerror(__method__,root,res,test_case)
end

# test_tree_p2max(tree1,"tree")

def test_tree_p2max_by(root,test_case)
	p "#{__method__}"
	res = root.p2max_by {|e| (e.val)**2}
	printerror(__method__,root.right,res,test_case)
end

# test_tree_p2max_by(tree1,"tree")

def test_tree_p2none?(root,test_case)
	p "#{__method__}"
	res = root.p2none? {|e| e.val > 10}
	printerror(__method__,true,res,test_case)
	
end

# test_tree_p2none?(tree1,"tree")

def test_tree_p2one?(root,test_case)
	p "#{__method__}"
	# res = root.p2one?
	# printerror(__method__,false,res,"array without block and have 2+ true value")
	# res = [1,4,nil,8].p2one?
	# printerror(__method__,false,res,"array without block and have 0 true value")
	res = root.p2one? {|e| e.val<=0}
	printerror(__method__,true,res,test_case)
	
end

# test_tree_p2one?(tree1,"tree with multiple match")
# test_tree_p2one?(tree1,"tree with only one match")
