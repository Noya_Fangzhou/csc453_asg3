module P2Enumerable


  def p2find_all
    r = []
    p2each do |e|
      if yield e
        r << e
      end
    end
    r
  end

  def p2reject
    r = []
    p2each do |e|
      if !(yield e)
        r << e
      end
    end
    r
  end


  def p2all?(&blk)
    r = true
    if blk==nil
      self.p2each do |e|
        r = r & e
      end
    else
    self.p2each do |e|
      if !(yield e)
        r = false
        break
      end
    end
  end
    return r
  end

  def p2any?(&blk)
    r = false
    if blk == nil
      self.p2each do |e|
        r = r | e 
      end
    else
      self.p2each do |e|
      if yield e
        r = true
        break
      end
    end
    end
    return r
  end

  def p2collect
    r = []
    p2each do |e|

      r << (yield e)
    end
    r
  end


  def p2collect_concat
    r = []
    p2each do |e|
   
      begin
      r += (yield e)
      rescue
      r << (yield e)
      end
    end
    return r
  end

  def p2drop(n)
    i = 0
    r = []
    p2each do |e|
      if i >= n
        r << e
      end
      i+=1
    end
    r
  end

  def p2first(n)
    i = 0
    r = []
    p2each do |e|
      if i < n
        r << e
      end
      i+=1
    end
    r
  end



  def p2count(&blk)
       r = 0
       self.p2each do |e|
           if blk == nil or yield e
               r += 1
           end
       end
       return r
    end





  def p2take(n)
    i = 0
    r = []
    p2each do |e|
      if i < n
        r << e
      end
      i+=1
    end
    r
  end

  def p2find_index
    r = nil
    i = 0
    p2each do |e|
      if yield e
        r = i
        break
      end
      i += 1
    end
    r
  end

  def p2take_while
    r = []
    p2each do |e|
      if yield e
        r << e
      else
        break
      end
    end
    r
  end

  def p2drop_while
    r = []
    stopDrop = false
    p2each do |e|
      if stopDrop
        r << e
      else
        if !(yield e)
          r << e
          stopDrop = true
        end
      end
    end
    return r
  end

  def p2inject(initial)
    r=initial
    p2each do |e|
      r=yield(r,e)
    end
    return r
  end

  def p2para0inject
    first = true
    r = nil
    self.each do |e|
      if first
        r = e
        first = false
      else
        r = yield( r , e )
      end
    end
    r
  end
   

  def p2cycle(n = nil,&blk)
    if n == nil
        p2each do |e|
          yield(e)
        end
        p2cycle(nil,&blk)
    end
      return nil if n == 0
      p2each do |e|
        yield(e)
      end
      if n>0
        p2cycle(n-1,&blk)
      end
    end

  
  def p2detect
    p2each do |e|
      if yield(e)
        return e
      end
    end
    return nil
  end

  def p2each_with_index(&blk)
    r=0
    p2each do |e|
      blk.call( e, r )
      r += 1
    end
  end

def p2find
  p2each do |e|
    if yield(e)
      return e
    end
  end
  return nil
end

def p2entries
  r=[]
  p2each do |e|
    r << e
   end
  return r
end

def p2to_a
  r=[]
  p2each do |e|
    r << e
  end
  return r
end


 def p2group_by(&blk)
   hash=Hash.new
   p2each do |e|
     r=[]
     p2each do|i|
      if blk.call(i) === blk.call(e)
        r << i
      end
    end
    hash[blk.call(e)] = r
  end
  return hash
 end

def p2minmax(&blk)
  if self.p2count == 0
    return [nil,nil]
  end
  i = true
  max1 = nil
  min1 = nil
  p2each do |e|
    if i == false
      max1 = e if blk.call(max1,e) < 0
      min1 = e if blk.call(min1,e) > 0
    end
    if i == true

      max1 = e
      min1 = e
      i = false
    end
  end
  return [min1,max1]
end

def p2minmax_by
  if self.p2count == 0
            return [nil, nil]
  end
 i = true
 max1 = 0
 min1 = 0
  p2each do |e|
  if i == true
   max1 = e  
   min1 = e 
   i = false
 else 
   max1 = e if yield(e) > yield(max1)
   min1 = e if yield(e) < yield(min1)
   end
  end
  return [min1,max1] 
end

def p2each_slice(n)
 #   i = 0
 #   r = []
 #   p2each do |e|
 #     if i % n == 0
 #       if i != 0
 #         yield r
 #       end
 #       r = []
 #     end
 #     r << e
 #     i += 1
 #   end
 #   if i >= n
 #     yield r
 #   end
 #   nil
 # end
    i=0
    r=[]
    len = self.p2count
    self.p2each do |e|
      j = 0
      self.p2each do |v|
        if i >=n*i && j<(n*(i+1))
          r<<v
        end
        j += 1
      end
      if i*n <=len
        yield r
      end
      i += 1
    end
  end


  def p2each_cons(para,&blk)
    temp = self
    isfirst = true
    s = 0
    self.p2each do |e|
      return nil if s == self.size-para+1
      temp = temp.p2drop(1) if isfirst == false
      temp = temp.p2drop(0) if isfirst == true
      isfirst = false
      r = temp[0,para]
      blk.call(r)
      s +=1
    end
  end


def p2partition(&blk)
  trueg = []
  falseg = []
  p2each do |e|
    trueg << e if blk.call(e)==true
    falseg << e if blk.call(e)==false
  end
  return [trueg,falseg]
end

def p2to_h(*args)
  hash = Hash.new
  p2each do | k , v|
    hash[k] = v
  end
  return hash
end

end
