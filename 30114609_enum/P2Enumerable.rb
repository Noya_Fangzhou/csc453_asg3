=begin

28 Enumerable methods implemented in Ruby anonymous function

Reference:
1. Enumerable API in Ruby Doc: http://ruby-doc.org/core-2.2.3/Enumerable.html#method-i-find_index
2. Understand 4 types of closure: blocks, Procs, lambdas and Methods: http://rubyer.me/blog/917/
3. CSC453 Class Demo
    
=end
module P2Enumerable

    # Passes each element of the collection to the given block. 
    # The method returns true if the block never returns false or nil

    def p2all?(&blk)
        self.p2each do |i|
            if blk == nil
                return false if i == nil
            elsif (not blk.call(i))
                return false
            end
        end
        return true
    end

    # Passes each element of the collection to the given block. 
    # The method returns true if the block ever returns a value other than false or nil. 

    def p2any?(&blk)
        self.p2each do |i|
            if (blk == nil)  
                return true if i != nil
            elsif blk.call(i)
               return true 
            end
        end
        return false
    end

    # Returns a new array with the results of running block once for every element in enum.

    def p2collect
        res = Array.new
        self.p2each do |i|
            temp = yield i
            res << temp
        end
        return res
    end 

   # Returns a new array with the concatenated results of running block once for every element in enum.

    def p2collect_concat
        res = Array.new
        self.p2each do |i|
            temp = yield(i)
            if temp.class == res.class
                res += temp
            else
                res << temp # {|e| e}报错
            end
        end
        return res
    end

    # Returns the number of items in enum through enumeration. 
    # If a block is given, it counts the number of elements yielding a true value.

    def p2count(&blk)
        res = 0
        self.p2each do |i|
            if blk == nil || (yield i)
                res += 1
            end
        end
        return res
    end

    # Calls block for each element of enum repeatedly n times or forever if none or nil is given. 
    # If a non-positive number is given or the collection is empty, does nothing. 
    # Returns nil if the loop has finished without getting interrupted.
    # cycle saves elements in an internal array so changes to enum after the first pass have no effect.

    def p2cycle(n, &blk)
        if (n < 1 || self.p2count == 0)
            return
        end
        self.p2each do |i|
                blk.call(i)
            end 
        if n==nil   
            self.p2cycle(nil, &blk)
        elsif n == 0
            return nil
        else
            self.p2cycle(n-1, &blk)
        end
    end

    # Passes each entry in enum to block. 
    # Returns the first for which block is not false. 
    # If no object matches, calls ifnone and returns its result when it is specified, or returns nil otherwise.

    def p2detect(ifnone = nil, &blk) 
        self.p2each do |i|
            if blk.call(i) 
                return i
            end
        end

        return ifnone

    end

    # Drops first n elements from enum, and returns rest elements in an array.

    def p2drop(n) 
        index = 0
        res = Array.new
        self.p2each do |i|
            if index>=n
                res << i
            end
            index += 1
        end
        return res
    end

                  

    def p2drop_while 
        isfirst = true
        res = Array.new
        self.p2each do |i|
            if not yield i
                isfirst = false
            end
            if not isfirst
                res << i
            end
        end
        return res
    end

    # Iterates the given block for each array of consecutive <n> elements

    def p2each_cons(n)
        len = self.p2count 
        index_i = 0
        self.p2each do |i|
            temp = Array.new()
            index_j = 0
            self.p2each do |j|
                if index_i+n <= len and index_j < n + index_i and index_j >= index_i
                    temp << j
                end
                index_j += 1
            end
            if index_i+n <= len
                yield temp 
                index_i += 1
            # else
            #     return 
            end
        end
    end

    # Iterates the given block for each slice of <n> elements.

    def p2each_slice(n)

        len = self.p2count
        index_i = 0
        self.p2each do |i|
            index_j = 0
            res = Array.new
            self.p2each do |j|
                if index_j >= n*index_i and index_j < (n*(index_i+1))
                    res << j
                end
                index_j += 1
            end
            if (index_i * n <= len)
                yield res
            end
            index_i += 1

        end
    end

    # Calls block with two arguments, the item and its index, for each item in enum. Given arguments are passed through to each().

    def p2each_with_index(*args) 
        index = 0
        self.p2each do |i|
            yield i, index, args
            index += 1
        end
    end

    # Returns an array containing the items in enum.
    
    def p2entries(*args) 
        return self.p2to_a
    end

    # Passes each entry in enum to block. 
    # Returns the first for which block is not false. 
    # If no object matches, calls ifnone and returns its result when it is specified, or returns nil otherwise.

    def p2find(ifnone,&blk)
        return self.p2detect(ifnone,&blk) 
    end

    # Returns an array containing all elements of enum for which the given block returns a true value.

    def p2find_all 
        res = Array.new
        self.p2each do |i|
            if yield i
                res << i
            end
        end
        return res
    end

    # Compares each entry in enum with value or passes to block. 
    # Returns the index for the first for which the evaluated value is non-false. If no object matches, returns nil

    def p2find_index
        index = 0
        self.p2each do |i|
            if yield i
                return index
            end
            index += 1
        end
        return nil
    end

    # Returns the first element, or the first n elements, of the enumerable. 
    # If the enumerable is empty, the first form returns nil, and the second form returns an empty array.

    def p2first(n) 
        if self.p2count == 0
            return []
        end
        count = 0
        res = Array.new
        self.p2each do |i|
            if count < n
                count += 1
                res << i
            end
        end
        return res

    end
    
    # Groups the collection by result of the block. 
    # Returns a hash where the keys are the evaluated result from the block and the values are arrays of elements in the collection that correspond to the key.

    def p2group_by
        res = Hash.new
        self.p2each do |i|
            temp = yield i
            if not res.has_key?(temp)
                val = self.p2find_all {|e| (yield e) == temp}
                res[temp] = val
            end   
        end
        return res
    end

    # Combines all elements of enum by applying a binary operation, specified by a block or a symbol that names a method or operator.
    # If you specify a block, then for each element in enum the block is passed an accumulator value (memo) and the element. 
    # In either case, the result becomes the new value for memo. 
    # At the end of the iteration, the final value of memo is the return value for the method.

    def p2inject(initial) 
        init = initial
        self.p2each do |i|
            init = yield i, init
        end
        return init
    end

    def p2para0inject 
        init = nil 
        isfirst = true
        self.p2each do |i|
            if isfirst
                if i.class == Hash.class
                    init = i[1]
                else
                    init = i
                end
                # init = i
                isfirst = false
            else
                puts init
                init = yield init, i
            end
        end
        return init
    end

    # Returns two elements array which contains the minimum and the maximum value in the enumerable. 
    # uses the block to return a <=> b.

    def p2minmax 
        if self.p2count == 0
           return [nil, nil] 
        end
        min = nil
        max = nil
        isfirst = true
        self.p2each do |i|
            if isfirst
                isfirst = false
                min = i
                max = i
            else
                if (yield i, min) < 0
                    min = i
                elsif (yield i, max) > 0
                    max = i
                end
            end
        end
        return [min,max]
    end


    # Returns a two element array containing the objects in enum that correspond to the minimum and maximum values respectively from the given block.

    def p2minmax_by
        if self.p2count == 0
            return [nil, nil]
        end
        min = 0
        minval = 0
        max = 0
        maxval = 0
        isfirst = true
        self.p2each do |i|
            temp = yield i
            if isfirst
                isfirst = false
                minval = temp
                maxval = temp
            end
            if temp <= minval 
                minval = temp
                min = i
            elsif temp > maxval
                maxval = temp   
                max = i               
            end
        end
        return [min,max]
    end

    # Returns two arrays, the first containing the elements of enum for which the block evaluates to true, the second containing the rest.

    def p2partition(&blk)
        trures = Array.new
        falres = Array.new
        trures = self.p2find_all(&blk)
        falres = self.p2reject(&blk)
        return [trures, falres]
    end

    # Returns an array for all elements of enum for which the given block returns false.

    def p2reject 
        res = Array.new
        self.p2each do |i|
            if not yield i
                res << i
            end
        end
        return res
    end


    # Returns first n elements from enum.

    def p2take(n) 
        self.p2first(n)
    end

    # Passes elements to the block until the block returns nil or false, then stops iterating and returns an array of all prior elements.

    def p2take_while 
        res = Array.new
        self.p2each do |i|
            if not yield i || (yield i) == nil
                return res
            else
                res << i
            end
        end
        return res
    end

    # Returns an array containing the items in enum. 

    def p2to_a(*args) 
        res = Array.new
        self.p2each do |i|
            res << i
        end
        return res
    end

    # Returns the result of interpreting enum as a list of [key, value] pairs.

    def p2to_h(*args) 
        res = Hash.new
        self.p2each do |key, val|
            res[key] = val
        end
        return res
    end

    # Return the object in enum with the minimum value. 

    def p2min(&blk)
        res = self.p2minmax &blk
        return res[0]
    end

    # Return n minimum object in enum 
    
    # def p2min(n)
    # end 
    
    # Return the object in enum that gives the minimum alue from the given block

    def p2min_by(&blk)
        res = self.p2minmax_by &blk
        return res[0]
    end 
    
    # Return n minimum object in enum 

    # def p2min_by(n)
    # end
    
    def p2max(&blk)
        res = self.p2minmax &blk
        return res[1]
    end

    # def p2max(n)
    # end
    
    def p2max_by(&blk)
        res = self.p2minmax_by &blk
        return res[1]
    end 
    
    # def p2max_by(n)
    # end 
    
    # Passes each element of the collection to the given block. 
    # The method returns true if the block never returns true for all elements. 
    # If the block is not given, none? will return true only if none of the collection members is true.

    def p2none?(&blk)
        if blk == nil
            res = self.p2find_all {|e| e == true}.p2count
            if res != 0
                return false
            else
                return true
            end
        else
            res = self.p2any? &blk
            return (not res)
        end
    end
    
    def p2one?(&blk)
        if blk == nil
            res = self.p2reject {|e| e == true}.p2count
            if res == 1
                return true
            else 
                return false
            end
        else
            res = self.p2find_all &blk
            if res.p2count == 1
                return true
            else
                return false
            end
        end

    end
    
    def p2sort 
    end

    def p2sort_by
    end




end