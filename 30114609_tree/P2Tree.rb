=begin
	
Reference
1. Ruby Lab: Binary Tree http://www.jianshu.com/p/1a4b420e4176

Acknowledgement
Thanks to my friend Yupeng Gou, who helps me find a small parameter scope issue in my p2_insert_node code
	
=end
require("./30114609_enum/P2Enumerable.rb")
class P2Tree

	include P2Enumerable
	# node value, the left children and right children respectively
	attr_accessor :val, :left, :right
	def initialize(value)
    	@val = value
  	end

	# enumerates each node of your tree (DLR)
	def p2each_tree(&blk)
		if self == nil
			return
		end
		blk.call(self)
		if self.left != nil
			self.left.p2each_tree(&blk)
		end
		if self.right != nil
			self.right.p2each_tree(&blk)
		end
	end

	def p2each_tree_level(level = 1, &blk)
		if self == nil
			return
		end
		blk.call(self, level)
		if self.left != nil
			# level += 1
			self.left.p2each_tree_level(level + 1, &blk)
		end
		if self.right != nil
			# level += 1
			puts level
			self.right.p2each_tree_level(level + 1, &blk)
		end
		# if self == nil
		# 	return nil
		# end
		# level = 1
		# isroot = true
		# hasboth  = nil
		# self.p2each_tree do |i|
		# 	blk.call(i,level)
		# 	if (i.left == nil) and (i.right == nil) and isroot
		# 		return
		# 	elsif (i.left != nil) and (i.right != nil)
		# 		isroot = false
		# 		level += 1
		# 		hasboth = true
		# 	elsif (i.left != nil) || (i.right != nil)
		# 		isroot = false
		# 		level += 1
		# 		hasboth = false
		# 	else
		# 		if not hasboth
		# 			level -= 1
		# 		else
		# 			hasboth = false
		# 		end
		# 	end
		# end
	end

	def p2insert_node(node)
		self.p2each_tree do |i|
			if i.left == nil
				i.left = node
				return
			elsif i.right == nil
				i.right = node
				return
			end
		end
	end
end
