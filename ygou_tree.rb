class P2Tree
	#initial a tree node.
	#content:the information contained in this tree node
	#left : the pointer to the leftson of this tree node
	#right : the pointer to the leftson of this tree node
	def initialize(content , left = nil , right = nil)
		@content = content
		@left = left
		@right = right
	end

	#insert a tree node to the left of the whole tree
	#arguments are the same of those above
	def insert(content ,left = nil , right = nil )		
		if @left == nil
			@left = P2Tree.new(content)
		else
			@left.insert(content)
		end
		
	end
	# get all tree nodes in DLR order
	def p2each(&block)
		if self == nil
			return
		end
		block.call(@content)
		@left.p2each(&block) if @left != nil
		@right.p2each(&block) if @right != nil 
	end
	# get all tree nodes with their levels in DLR order
	def p2each_with_level(level = 1,&block)
		if self == nil
			return
		end
		block.call(@content,level)
		@left.p2each_with_level(level + 1, &block) if @left != nil
		@right.p2each_with_level(level + 1, &block) if @right != nil
	end
end
