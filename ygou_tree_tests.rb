load 'ygou_tree.rb'

#test1 :1 2 3 4 nil nil 5 6 7
test1_leaf1 = P2Tree.new(6)
test1_leaf2 = P2Tree.new(7)
test1_leaf3 = P2Tree.new(5)
test1_brach3 = P2Tree.new(4,test1_leaf1,test1_leaf2)
test1_brach2_1 = P2Tree.new(2,test1_brach3)
test1_brach2_2 = P2Tree.new(3,nil,test1_leaf3)
@test1_root = P2Tree.new(1,test1_brach2_1,test1_brach2_2)

#test2 : 1 2 3 4 5 6 7
test2_leaf1 = P2Tree.new(4)
test2_leaf2 = P2Tree.new(5)
test2_leaf3 = P2Tree.new(6)
test2_leaf4 = P2Tree.new(7)
test2_brach1 = P2Tree.new(2,test2_leaf1,test2_leaf2)
test2_brach2 = P2Tree.new(3,test2_leaf3,test2_leaf4)
@test2_root = P2Tree.new(1,test2_brach1,test2_brach2)

#test3 : 1 2 nil 3 nil 4 nil
test3_leaf = P2Tree.new("4")
test3_brach2 = P2Tree.new("3",test3_leaf)
test3_bracn1 = P2Tree.new("2",test3_brach2)
@test3_root = P2Tree.new("1",test3_bracn1)

#test4 : 1 nil 2 nil 3 nil 4
test4_leaf = P2Tree.new(4)
test4_brach2 = P2Tree.new(3,nil,test4_leaf)
test4_bracn1 = P2Tree.new(2,nil,test4_brach2)
@test4_root = P2Tree.new(1,nil,test4_bracn1)


def test_Tree_p2each
	test1 = Array.new
	@test1_root.p2each{|e| test1 << e}
	raise "#{__method__} error" if test1 != [1,2,4,6,7,3,5]
	test2 = Array.new
	@test2_root.p2each{|e| test2 << e}
	raise "#{__method__} error" if test2 != [1,2,4,5,3,6,7]
	test3 = Array.new
	@test3_root.p2each{|e| test3 << e}
	raise "#{__method__} error" if test3 != ["1","2","3","4"]
	test4 = Array.new
	@test4_root.p2each{|e| test4 << e}
	raise "#{__method__} error" if test4 != [1,2,3,4]
	p "#{__method__} passed"
	
end

def test_Tree_p2each_with_level
	test1 = Hash.new
	@test1_root.p2each_with_level{|e,level| test1[e] = level}
	raise "#{__method__} error" if test1 != {1=>1,2=>2,4=>3,6=>4,7=>4,3=>2,5=>3}
	test2 = Hash.new
	@test2_root.p2each_with_level{|e,level| test2[e] = level}
	raise "#{__method__} error" if test2 != {1=>1,2=>2,4=>3,5=>3,3=>2,6=>3,7=>3}
	test3 = Hash.new
	@test3_root.p2each_with_level{|e,level| test3[e] = level}
	raise "#{__method__} error" if test3 != {"1"=>1,"2"=>2,"3"=>3,"4"=>4}
	test4 = Hash.new
	@test4_root.p2each_with_level{|e,level| test4[e] = level}
	raise "#{__method__} error" if test4 != {1=>1,2=>2,3=>3,4=>4}
	p "#{__method__} passed"
end

test_Tree_p2each
test_Tree_p2each_with_level