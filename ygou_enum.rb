module P2Enumerable
	#1	p2all?
	def p2all?(&block)
		ans = true
		if block == nil
			self.p2each do |e|
				ans = ans & e
			end
		else
			self.p2each do |e|
				ans = ans & yield(e)
			end
		end
		return ans
	end
	#2	p2any?
	def p2any?(&block)
		ans = false
		if block == nil
			self.p2each do |e|
				ans = ans | e
			end
		else
			self.p2each do |e|
				ans = ans | yield(e)
			end
		end
		return ans
	end
	#3	p2collect
	def p2collect(&block)
		ans = Array.new
		self.p2each do |e|
			tmp = yield (e)
			ans << tmp
		end
		return ans
	end
	#4	
	def p2collect_concat(&block)
		ans = Array.new 
		self.p2each do |e|
			tmp = yield(e)				
			begin
				ans = ans +  tmp
			rescue Exception 
				ans << tmp
			end
		end
		return ans
	end
	#5
	def p2count(&block)
		if block == nil
			return self.size
		end
		ans = 0
		self.p2each do |e|
			if yield(e)
				ans = ans + 1
			end
		end
		return ans
	end
	#6
	def p2cycle( n = nil , &block)
		if (n == nil)
			self.p2each do |e|
				yield(e)
			end
			return p2cycle(n,&block)
		else
			if n <= 0
				return nil
			else
				return nil if n == 0
				self.p2each do |e|
					yield(e)
				end
				n -= 1
				return p2cycle(n,&block)
			end
		end
	end
	#7 
	def p2detect(ifnone = nil , &block )
		num = 0
		self.each do |e|
			if yield (e)
				return e
			end
			num = num + 1
		end
		return nil
	end
	#8
	def p2drop(n)
		ans = Array.new
		num = 0
		self.p2each do |e|
			if num >= n
				ans << e
			end
			num +=1
		end
		return ans
	end
	#9 drop_while
	def p2drop_while(&block)
		ans = Array.new
		flag = false
		self.p2each do |e|
			if flag
				ans << e
			else
				if not yield(e)
					flag = true
					ans << e
				end
			end
		end
		return ans
	end
	#10 *array*
	def p2each_cons(n ,&block)
		num = 0
		len = 0
		self.p2each do |e|
			len += 1
		end
		self.p2each do |e|
			if num + n <= len
				tmp_num = 0
				tmp_array = Array.new
				self.p2each do |e|
					if tmp_num >= num and tmp_num < num + n
						tmp_array << e
					end
					tmp_num += 1
				end
				yield(tmp_array)
			end
			num += 1
		end
	end
	#11 array
	def p2each_slice(n , &block)
		num = 0
		len = 0
		self.p2each do |e|
			len += 1
		end
		self.p2each do |e|
			if num >= len
				return
			else
				tmp_num = 0
				tmp_array = Array.new
				self.p2each do |e|
					if tmp_num >= num and tmp_num < num + n
						tmp_array << e
					end
					tmp_num += 1
				end
				yield(tmp_array)
				num = num + n
			end
		end
	end
	#12
	def p2each_with_index(*arg , & block)
		num = 0
		self.p2each do |e|
			yield(e,num)
			num += 1
		end
	end
	#13
	def p2entries 
		ans = Array.new
		self.p2each do |e|
			ans << e
		end
		return ans
	end

	#14
	alias p2find p2detect

	#15
	def p2find_all(&block)
		ans = Array.new
		self.p2each do |e|
			if yield(e)
				ans << e
			end
		end
		return ans
	end
	#16
	def p2find_index(&block)
		num = 0
		self.p2each do |e|
			if yield(e)
				return num
			end
			num +=1
		end
		return nil
	end
	#17
	def p2first(n)
		num = 0
		ans = Array.new
		self.p2each do |e|
			if num < n
				ans << e
			end
			num = num + 1
		end
		return ans 
	end
	#18
	def p2group_by(&block)
		ans = Hash.new
		self.p2each do |e|
			flag = false
			tmp_ans = Hash.new
			tmp = yield(e)
			ans.each do |key ,value|
				if flag == true
					tmp_ans[key] = value
				else
					if key == tmp
						tmp_ans[key] = (value << e)
						flag = true
					else
						tmp_ans[key] = value
					end
				end 
			end
			if flag == false
				tmp_ans[tmp] = [e]
			end
			ans = tmp_ans
		end
		return ans
	end
	#19 reference to Chen Ding's example
	def p2inject(initial , &block)
		ans = initial
		self.p2each do |e|
			ans = yield(ans , e)
		end
		return ans
	end
	#20 reference to Chen Ding's example
	def p2para0inject(&block)
		flag = false
		ans = nil
		self.p2each do |e|
			if flag == false
				ans = e
				flag = true
			else
				ans = yield(ans ,e)
			end
		end
		return ans
	end
	#21
	def p2minmax(&block)
		min = nil
		max = nil
		flag = false
		self.p2each do |e|
			if  flag == false
				min = e
				max = e
				flag = true
			else
				if yield(e,max) == 1
					max = e
				end
				if yield(e,min) == -1
					min = e
				end
			end
		end
		return [min,max]
	end
	#22
	def p2minmax_by(&block)
		flag = false
		min = nil
		max = nil
		ans_min = nil
		ans_max = nil
		self.p2each do |e|
			tmp = yield(e)
			if flag == false
				min = tmp
				max = tmp
				ans_min = e
				ans_max = e
				flag = true
			else
				if tmp < min
					min = tmp
					ans_min = e
				end
				if tmp > max
					max = tmp
					ans_max = e
				end
			end
		end
		return [ans_min,ans_max]
	end
	#23
	def p2partition(&block)
		part1 = Array.new
		part2 = Array.new
		self.p2each do |e|
			if yield(e) == true
				part1 << e
			else
				part2 << e
			end
		end
		return [part1,part2]
	end

	#24
	def p2reject(&block)
		ans = Array.new
		self.p2each do |e|
			if yield(e) == false
				ans << e
			end
		end
		return ans
	end
	
	#25
	alias p2take p2first

	#26
	def p2take_while(&block)
		ans = Array.new
		self.p2each do |e|
			tmp = yield(e)
			if tmp == false
				return ans
			else
				ans << e
			end
		end
		return ans
	end

	#27
	alias p2to_a p2entries

	#28
	def p2to_h(*args)
		ans = Hash.new
		self.p2each do |e|
			key = e[0]
			value = e[1]
			ans[key] = value
		end
		return ans
	end

	#Test method only for to_h in a Tree#
	def p2each_with_level_to_h
		ans = Hash.new
		self.p2each_with_level do |x,y|
			key = x
			value = y
			ans[key] = value
		end
		return ans
	end

	#29
	def p2para0min(&block)
		flag = false
		ans = nil
		self.p2each do |e|
			if flag === false
				ans = e
				flag = true
			else
				if yield(e,ans) == -1
					ans = e
				end
			end
		end
		return ans
	end
	
	#30
	def p2para0max(&block)
		ans = nil
		flag = false
		self.p2each do |e|
			if flag == false
				ans = e
				flag = true
			else
				if yield(e,ans) == 1
					ans = e
				end
			end
		end
		return ans
	end
	#31
	def p2one?(&block)
		if block == nil
			num1 = 0
			self.p2each do |e|
				if e == true
					num1 += 1
				end
			end
			if num1 == 1
				return true
			else
				return false
			end
		else
			num2 = 0
			self.p2each do |e|
				if yield(e) == true
					num2 += 1
				end
			end
			
			if num2 == 1
				return true
			else
				return false
			end
		end
	end
	#32
	def p2none?(&block)
		if block == nil
			self.p2each do |e|
				if e == true
					return false
				end
			end
			return true
		else
			self.p2each do |e|
				if yield(e) == true
					return false
				end
			end
			return true
		end
	end
end