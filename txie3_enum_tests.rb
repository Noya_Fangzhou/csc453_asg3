load 'txie3_enum.rb'

class Array
  include P2Enumerable
  alias p2each each
end

class Hash
  include P2Enumerable
  alias p2each each
end

class Range
  include P2Enumerable
  alias p2each each
end



def test_p2findall
  r = [1, 2, 3, 2].p2find_all { |e| e == 2 }
  raise "#{__method__} error" if r != [2, 2]
  p "#{__method__} passed"
end

def test_p2findallH
  r = {1=>1, 2=>2, 3=>3}.p2find_all { |e| e[1] == 2 }
  raise "#{__method__} error" if r != [[2, 2]]
  p "#{__method__} passed"
end

def test_p2reject
  r = [1, 2, 3, 2].p2reject { |e| e == 2 }
  raise "#{__method__} error" if r != [1, 3]
  p "#{__method__} passed"
end

def test_p2rejectH
  r = {1=>1, 2=>2, 3=>3}.p2reject { |e| e[1] == 2 }
  # puts r.inspect
  raise "#{__method__} error" if r != [[1, 1], [3, 3]]
  p "#{__method__} passed"
end

def test_p2all1
  r = [100, 200, 300].p2all? { |e| e >= 100 }
  raise "#{__method__} error" if r != true
  p "#{__method__} passed"
end

def test_p2all1H
  r = {1=>10, 2=>20, 3=>30}.p2all? { |e| e[0] >= 1 }
  raise "#{__method__} error" if r != true
  p "#{__method__} passed"
end

def test_p2all2
  r = [100, 200, 300].p2all? { |e| e >= 200 }
  raise "#{__method__} error" if r != false
  p "#{__method__} passed"
end

def test_p2all2H
  r = {1=>10, 2=>20, 3=>30}.p2all? { |e| e[0] >= 200 }
  raise "#{__method__} error" if r != false
  p "#{__method__} passed"
end

def test_p2any1
  r = [100, 200, 300].p2any? { |e| e >= 100 }
  raise "#{__method__} error" if r != true
  p "#{__method__} passed"
end

def test_p2any2
  r = [100, 200, 300].p2any? { |e| e >= 400 }
  raise "#{__method__} error" if r != false
  p "#{__method__} passed"
end

def test_p2any1H
  r = {100 => 0, 200 => 1, 300=>2}.p2any? { |e| e[0] >= 100 }
  raise "#{__method__} error" if r != true
  p "#{__method__} passed"
end

def test_p2any2H
  r = {100 => 0, 200 => 1, 300=>2}.p2any? { |e| e[1] >= 400 }
  raise "#{__method__} error" if r != false
  p "#{__method__} passed"
end


def test_p2collect
  r = [100, 200].p2collect { |e| -e }
  raise "#{__method__} error" if r != [-100, -200]
  p "#{__method__} passed"
end

def test_p2collect_concat

  r = [1, 3, 4].p2collect_concat { |e| [e] }
  raise "#{__method__} error" if r != [1, 3, 4]
  p "#{__method__} passed"
end

def test_p2collectH
  r = {100=>1, 200=>2}.p2collect { |e| -e[0] }
  raise "#{__method__} error" if r != [-100, -200]
  p "#{__method__} passed"
end

def test_p2collect_concatH

  r = {1=>2, 3=>4, 4=>5}.p2collect_concat { |e| e }
  raise "#{__method__} error" if r != [1, 2,3, 4, 4, 5]
  p "#{__method__} passed"
end


def test_p2drop1
  r = [1, 2, 3, 4, 5, 0].p2drop(3)
  raise "#{__method__} error" if r != [4, 5, 0]
  p "#{__method__} passed"
end

def test_p2drop1H
  r = {1=>0, 2=>1, 3=>2, 4=>3, 5=>4, 0=>5}.p2drop(3)
  raise "#{__method__} error" if r != [[4, 3], [5, 4], [0, 5]]
  p "#{__method__} passed"
end

def test_p2drop2
  r = [1, 2, 3, 4, 5, 0].p2drop(10)
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2drop2H
  r = {1=>1, 2=>3, 3=>4, 4=>5, 5=>6, 0=>3}.p2drop(10)
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2first1
  r = [100, 200, 300].p2first(2)
  raise "#{__method__} error" if r != [100, 200]
  p "#{__method__} passed"
end

def test_p2first2
  r = [100, 200, 300].p2first(10)
  raise "#{__method__} error" if r != [100, 200, 300]
  p "#{__method__} passed"
end

def test_p2first1H
  r = {100=>1, 200=>2, 300=>3}.p2first(2)
  raise "#{__method__} error" if r != [[100, 1], [200, 2]]
  p "#{__method__} passed"
end

def test_p2first2H
  r = {100=>1, 200=>2, 300=>3}.p2first(10)
  raise "#{__method__} error" if r != [[100, 1], [200,2], [300,3]]
  p "#{__method__} passed"
end

def test_p2count
  r = [1, 2, 3, 4].p2count { |x| x%2==1 }
  raise "#{__method__} error" if r != 2
  p "#{__method__} passed"
end

def test_p2take1
  r = [].p2take(2)
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2take2
  r = [1, 2, 3].p2first(2)
  raise "#{__method__} error" if r != [1, 2]
  p "#{__method__} passed"
end


def test_p2findindex1
  r = [1, 2, 3, 2].p2find_index { |e| e == 2 }
  raise "#{__method__} error" if r != 1
  p "#{__method__} passed"
end

def test_p2findindex2
  r = [1, 2, 3, 2].p2find_index { |e| e == 4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2take_while
  r = [1, 2, 3, 2].p2take_while { |e| e < 3 }
  raise "#{__method__} error" if r != [1, 2]
  p "#{__method__} passed"
end

def test_p2drop_while
  r = [1, 2, 3, 2].p2drop_while { |e| e < 3 }
  raise "#{__method__} error" if r != [3, 2]
  p "#{__method__} passed"
end



def test_p2countH
  r = {1=>1, 2=>3, 3=>4, 4=>1}.p2count { |x| x[0]%2==1 }
  raise "#{__method__} error" if r != 2
  p "#{__method__} passed"
end

def test_p2take1H
  r = {}.p2take(2)
  raise "#{__method__} error" if r != []
  p "#{__method__} passed"
end

def test_p2take2H
  r = {1=>0, 2=>1, 3=>2}.p2first(2)
  raise "#{__method__} error" if r != [[1, 0], [2, 1]]
  p "#{__method__} passed"
end

def test_p2findindex1H
  r = {1=>2, 2=>1, 3=>1, 4=>2}.p2find_index { |e| e[0] == 2 }
  raise "#{__method__} error" if r != 1
  p "#{__method__} passed"
end

def test_p2findindex2H
  r = {1=>2, 2=>1, 3=>1, 5=>2}.p2find_index { |e| e[0] == 4 }
  raise "#{__method__} error" if r != nil
  p "#{__method__} passed"
end

def test_p2take_whileH
  r = {1=>2, 2=>1, 3=>1, 4=>2}.p2take_while { |e| e[0] < 3 }
  raise "#{__method__} error" if r != [[1, 2], [2, 1]]
  p "#{__method__} passed"
end

def test_p2drop_whileH
  r = {1=>1, 2=>2, 3=>4, 5=>1}.p2drop_while { |e| e[0] < 3 }
  raise "#{__method__} error" if r != [[3,4], [5, 1]]
  p "#{__method__} passed"
end

def test_p2inject
  r = [1,2,3].p2inject(0){ |s, e| s + e }
  raise "#{__method__} error" if r != 6
  p "#{__method__} passed"
end



def test_p2para0inject
  r = [5,6,7,8,9].p2para0inject { |product, n| product * n }
  raise "#{__method__} error" if r != 15120
  p "#{__method__} passed"
end


def test_p2detect
  r=[1,5,7,35,30,42,70].p2detect { |e| e % 6 == 0 and e % 7 == 0}
  raise "#{__method__} error" if r != 42
  p "#{__method__} passed"
end

def test_p2each_with_index
  hash = Hash.new
  %w(apple pear banana).p2each_with_index {|item, index| hash[item] = index}
  raise "#{__method__} error" if hash!={"apple"=>0, "pear"=>1, "banana"=>2}
  p "#{__method__} passed"
end

def test_p2find
 r=[10,20,30].p2find { |e| e % 10 == 0}
 raise "#{__method__} error" if r != 10
 p "#{__method__} passed"
end

def test_p2entries
  r=(1..10).p2entries
  raise "#{__method__} error" if r!=[1,2,3,4,5,6,7,8,9,10]
  p "#{__method__} passed"
end

def test_p2to_a
  r={'d'=>10,'e'=>20, 'f'=>30}.p2to_a
  raise "#{__method__} error" if r!=[["d",10],["e",20],["f",30]]
  p "#{__method__} passed"
end

 def test_gruop_by
  r=(1..10).p2group_by{|i| i % 2}
  raise "#{__method__} error" if r!={0=>[2,4,6,8,10],1=>[1,3,5,7,9]}
  p "#{__method__} passed"
end

def test_p2minmax
  r=%w(apple pear banana).p2minmax{|a,b| a.length <=> b.length}
  raise "#{__method__} error" if r!=["pear","banana"]
  p "#{__method__} passed"
end

def test_p2minmax_by
	r=%w(apple pear banana).p2minmax_by{|a| a.length}
	raise "#{__method__} error" if r!=["pear","banana"]
	p "#{__method__} passed"
end
# def test_p2to_h
#   r=%i[i love apple].each_with_index.p2to_h
#   raise "#{__method__} error" if r!={0=>"i",1=>"love",2=>"apple"}
#   p "#{__method__} passed"
# end

def test_p2each_slice
  x = []
  r = [1, 2, 3, 4].p2each_slice(3) { |e| x << e }
  raise "#{__method__} error" if x != [[1, 2, 3], [4]] || r != nil
  p "#{__method__} passed"
end

def test_p2each_slice2
  x = []
  r = [1, 2, 3, 4].p2each_slice(5) { |e| x << e }
  raise "#{__method__} error" if x != [] || r != nil
  p "#{__method__} passed"
end

def test_p2each_sliceH
  x = []
  r =  {1=>1, 2=>2, 3=>3, 4=>4}.p2each_slice(3) { |e| x << e }
  raise "#{__method__} error" if x != [[[1, 1], [2, 2], [3, 3]], [[4, 4]]] || r != nil
  p "#{__method__} passed"
end

def test_p2each_sliceH2
  x = []
  r = {1=>1, 2=>2, 3=>3, 4=>4}.p2each_slice(5) { |e| x << e }
  raise "#{__method__} error" if x != [] || r != nil
  p "#{__method__} passed"
end

def test_p2partition
 r = (1..10).p2partition {|e| e.even? }
 raise "#{__method__} error" if r!=[[2,4,6,8,10],[1,3,5,7,9]]
 p "#{__method__} passed"
end

test_p2findall

test_p2reject

test_p2all1

test_p2all2

test_p2any1

test_p2any2

test_p2collect

test_p2findallH

test_p2rejectH

test_p2all1H

test_p2all2H

test_p2any1H

test_p2any2H

test_p2collectH

test_p2collect_concatH

test_p2first1H

test_p2drop1H

test_p2drop2H

test_p2first1H

test_p2first2H

test_p2countH

test_p2take1H

test_p2take2H

test_p2findindex1H

test_p2findindex2H

test_p2take_whileH

test_p2drop_whileH

test_p2inject

# test_p2injectH

test_p2para0inject

# test_p2cycle

test_p2detect

test_p2each_with_index

test_p2find

test_p2entries

test_p2to_a

test_gruop_by

test_p2minmax

test_p2minmax_by

test_p2each_slice

test_p2each_slice2

test_p2each_sliceH

test_p2each_sliceH2

test_p2partition