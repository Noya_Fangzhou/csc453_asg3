=begin
	
Unit test for Tree 
	
=end

# require("./30114609_tree/P2Tree.rb")
require("./txie3_tree.rb")

# used to print the detail of the test case

def printerror(methodName, expected, actual, classname)
	raise "#{methodName} for #{classname} error\nexpected #{expected} actual is #{actual}" if actual != expected
	puts "#{methodName} for #{classname} passed"
end

=begin
	
The structure of the binary tree used in this test is 

										-------
										|  0  |
										-------
							-------        |    	-------
							|  1  |	---------------	|  2  |
							-------				    -------
				-------		   |        -------        |        -------
				|  3  |	--------------- |  4  |		 	--------|  5  |
				-------					-------					-------

=end

# tree1 = P2Tree.new(0)
# tree1.left = P2Tree.new(1)
# tree1.right = P2Tree.new(2)
# tree1.left.left = P2Tree.new(3)
# tree1.left.right = P2Tree.new(4)
# tree1.right.right = P2Tree.new(5)
llc2 = P2Tree.new(3,3,nil,nil)
lrc2 = P2Tree.new(4,3,nil,nil)
rrc2 = P2Tree.new(5,3,nil,nil)
lc1 = P2Tree.new(1,2,llc2,lrc2)
rc1 = P2Tree.new(2,2,nil,rrc2)
tree1 = P2Tree.new(0,1,lc1,rc1)



tree2 = P2Tree.new("a",1,nil,nil)

# def test_tree_p2each(tree,expected_result,test_case)
# 	res = Array.new()
# 	tree.p2each_tree {|node| res << node.val}
# 	printerror(__method__,expected_result,res,test_case)
# end
def test_tree_p2each(tree,expected_result,test_case)
	res = Array.new()
	tree.p2each {|node| res << node.data}
	printerror(__method__,expected_result,res,test_case)
end

# test_tree_p2each(tree1,[0,1,3,4,2,5],"tree enumerator")
# test_tree_p2each(tree2,["a"],"tree with only root node")

# def test_tree_p2each_tree_level(tree,expected_result,test_case)
# 	res = Hash.new()
# 	tree.p2each_tree_level {|node, l| res[node.val] = l}
# 	printerror(__method__,expected_result,res,test_case)
# end
def test_tree_p2each_tree_level(tree,expected_result,test_case)
	res = Hash.new()
	tree.p2each_with_level {|node, l| res[node.data] = l}
	printerror(__method__,expected_result,res,test_case)
end

# test_tree_p2each_tree_level(tree1,{0=>1, 1=>2, 3=>3, 4=>3, 2=>2, 5=>3},"tree level enumerator")

=begin
	
The structure of the binary tree used in this test is 

										-------
										|  0  |
										-------
							-------        |    	-------
							|  1  |	---------------	|  2  |
							-------				    -------
				-------		   |        -------        |        -------
				|  3  |	--------------- |  4  |		 	--------|  5  |
				-------					-------					-------


The structure of the binary tree after insertation in this test is 

										-------
										|  0  |
										-------
							-------        |    	-------
							|  1  |	---------------	|  2  |
							-------				    -------
				-------		   |        -------        |        -------
				|  3  |	--------------- |  4  |		 	--------|  5  |
				-------					-------					-------
	-------		   |
	|  6  | -------
	-------
=end
def test_tree_p2insert_node(tree,expected_result,test_case)
	res = Hash.new()
	tree.p2insert_node(P2Tree.new(6))
	tree.p2each_tree_level {|node, l| res[node.val] = l}
	printerror(__method__,expected_result,res,test_case)
end

# test_tree_p2insert_node(tree1,{0=>1, 1=>2, 3=>3, 6=>4, 4=>3, 2=>2, 5=>3},"insert a node to the tree")


