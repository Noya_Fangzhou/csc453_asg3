require("./txie3_tree.rb")

T7 = P2Tree.new(10,4,nil,nil)
T8 = P2Tree.new(1,4,nil,nil)
T4 = P2Tree.new(4,3,nil,nil)
T5 = P2Tree.new(5,3,nil,nil)
T6 = P2Tree.new(3,3,nil,nil)
T2 = P2Tree.new(6,2,nil,nil)
T3 = P2Tree.new(9,2,nil,nil)
T1 = P2Tree.new(8,1,nil,nil)

T1.insert T2,T3
T2.insert T4,T5
T3.insert T6,nil
T4.insert T7,nil
T5.insert nil,T8

def test_p2each(tree)
	r=Array.new()
	tree.p2each {|e| r << e.dat}
	p r
	raise"#{__method__} error" if r != [8,6,4,10,5,1,9,3]
	p "#{__method__} passed"
end

test_p2each(T1)


def test_p2each_with_level(tree)
	r=[]
	tree.p2each_with_level {|e| r<<[e.dat, e.level]}
	raise"#{__method__} error" if r!=[[8,1],[6,2],[4,3],[10,4],[5,3],[1,4],[9,2],[3,3]]
	p"#{__method__} passed"
end

test_p2each_with_level(T1)




