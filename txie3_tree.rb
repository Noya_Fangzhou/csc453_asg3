class P2Tree  
  attr_accessor :dat, :level  
  def initialize(data,level,lChild,rChild)  
    @dat = data
    @level=level  
  end 
 
  def p2each(&block)
    block.call(self)
    if @lChild then
      @lChild.p2each(&block)
    end
    if @rChild then
      @rChild.p2each(&block)
    end
  end

  def p2each_with_level(&block)
    block.call(self,@level)
    if @lChild!=nil then
      @lChild.p2each_with_level(&block)
    end
    if @rChild!=nil then
      @rChild.p2each_with_level(&block)
    end 
    # print "value:#@dat level: #@level"
    # print "\n"
  end

  def insert(node1,node2)
    @lChild=node1
    @rChild=node2
  end
end

# T7 = P2Tree.new(10,4,nil,nil)
# T8 = P2Tree.new(1,4,nil,nil)
# T4 = P2Tree.new(4,3,nil,nil)
# T5 = P2Tree.new(5,3,nil,nil)
# T6 = P2Tree.new(3,3,nil,nil)
# T2 = P2Tree.new(6,2,nil,nil)
# T3 = P2Tree.new(9,2,nil,nil)
# T1 = P2Tree.new(8,1,nil,nil)

# T1.insert T2,T3
# T2.insert T4,T5
# T3.insert T6,nil
# T4.insert T7,nil
# T5.insert nil,T8

 #print "p2each funciton\n"
# print T1.each {|x| puts x.data} 
# print "p2each_with_level function\n"
# puts T1.p2each_with_level
