load 'ygou_enum.rb'

class Array
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end


#1. 
def test_p2all?
	s = {'1' => 1, '2' => 2,'3' => 3}.p2all?
	raise "#{__method__} error" if s != true
	s = {'1' => 1, '2' => 2,'3' => 3}.p2all?{|key| key == '1'}
	raise "#{__method__} error" if s != false
	s = {'41' => 1, '2' => 2, "32" => 3}.p2all?{|key| key.length > 1}
	raise "#{__method__} error" if s != true
	s = {"41" => 1, "2" => 2, "32" => 3}.p2all?{|key| key.length > 2}
	raise "#{__method__} error" if s != false
	s ={}.all?
	raise "#{__method__} error" if s != true
	s = ["test1","test2","testa"].p2all?{|e| e.length == 5}
	raise "#{__method__} error" if s != true
	s = ["test1","test2","testall"].p2all?{|e| e.length > 4}
	raise "#{__method__} error" if s != true
	s =[].p2all?
	raise "#{__method__} error" if s != true
	s =[2,4,6,8].all?{|e| e % 2 == 0}
	raise "#{__method__} error" if s != true
	s =[2,4,6,8,9].all?{|e| e % 2 == 0}
	raise "#{__method__} error" if s != false
	#test from document
	s = [1,2,3].p2all?
	raise "#{__method__} error" if s != true
	s = %w[ant bear cat].p2all?{ |word| word.length >= 3}
	raise "#{__method__} error" if s != true
	s = %w[ant bear cat].p2all?{ |word| word.length >= 4}
	raise "#{__method__} error" if s != false
	s = [nil, true , 99].p2all?
	raise "#{__method__} error" if s != false
	s = [23, true , true].p2all?
	raise "#{__method__} error" if s != true
	p "#{__method__} passed"
end

#2
def test_p2any?
	s ={'1' => 1, '2' => 2,'3' => 3}.p2any?
	raise "#{__method__} error" if s != true
	s ={}.p2any?
	raise "#{__method__} error" if s != false
	s ={'1' => 1, '2' => 2,'3' => 3}.p2any?{|key| key == '3'}
	raise "#{__method__} error" if s != false
	s ={'12' => 1, '22' => 2,'32' => 3}.p2any?{|key| key.length > 1}
	raise "#{__method__} error" if s != true
	s =[].p2any?
	raise "#{__method__} error" if s != false
	s =[1,2,3,4].p2any?
	raise "#{__method__} error" if s != true
	s =[1,2,3,4].p2any?{|e| e % 2 == 0}
	raise "#{__method__} error" if s != true
	s =[1,2,3,4].p2any?{|e| e % 5 == 0}
	raise "#{__method__} error" if s != false
	#test from document
	s = [nil, nil, nil].p2any?
	raise "#{__method__} error" if s != false
	s = [1 ,true, nil, nil].p2any?
	raise "#{__method__} error" if s != true
	s = %w[ant bear cat].p2any?{ |word| word.length >= 3}
	raise "#{__method__} error" if s != true
	s = %w[ant bear cat].p2any?{ |word| word.length >= 4}
	raise "#{__method__} error" if s != true
	s = %w[ant bear cat].p2any?{ |word| word.length >= 5}
	raise "#{__method__} error" if s != false
	p "#{__method__} passed"
end
#3
def test_p2collect
	s =[1,2,3,4].p2collect{|e| e + 1}
	raise "#{__method__} error" if s != [2,3,4,5]
	s =["1","2","3","4"].p2collect{|e| "assignment" + e}
	raise "#{__method__} error" if s != ["assignment1", "assignment2", "assignment3", "assignment4"]
	s =[].p2collect{|e| e * e}
	raise "#{__method__} error" if s != []
	s ={'1' => 1, '2' => 2,'3' => 3}.p2collect{|key,value| value + 2}
	raise "#{__method__} error" if s != [3,4,5]
	s ={'1' => 1, '2' => 2,'3' => 3}.p2collect{|key,value| key + "test"}
	raise "#{__method__} error" if s != ["1test", "2test", "3test"]
	p "#{__method__} passed"
end
#4

def test_p2collect_concat
	s = [1,2,3,4].p2collect_concat{|e| e}
	raise "#{__method__} error" if s != [1, 2, 3, 4]
	s = [1,2,3,4].p2collect_concat{|e| [e]}
	raise "#{__method__} error" if s != [1, 2, 3, 4]
	s = [1,3,5,7].p2collect_concat{|e| [e,100]}
	raise "#{__method__} error" if s != [1, 100, 3, 100, 5, 100, 7, 100]
	s = [].p2collect_concat{|e| [e,100]}
	raise "#{__method__} error" if s != []
	s = {1=>'1',2=>'2',3=>'3',4=>'4'}.p2collect_concat{|e| [e]}
	raise "#{__method__} error" if s != [[1, "1"], [2, "2"], [3, "3"], [4, "4"]]
	s = {1=>'1',2=>'2',3=>'3',4=>'4'}.p2collect_concat{|e| [e] + [100]}
	raise "#{__method__} error" if s != [[1, "1"], 100, [2, "2"], 100, [3, "3"], 100, [4, "4"], 100]
	s = {}.p2collect_concat{|e| [e,100]}
	raise "#{__method__} error" if s != []
	#Test from Document
	s = [1,2,3,4].p2collect_concat{|e| [e,-e]}
	raise "#{__method__} error" if s != [1, -1, 2, -2, 3, -3, 4, -4]
	p "#{__method__} passed"
end
#5
def test_p2count
	s ={'1' => 1, '2' => 2,'3' => 3}.p2count{|key,value| key == '1'}
	raise "#{__method__} error" if s != 1
	s ={'1' => 1, '2' => 2,'3' => 3}.p2count{|key,value| key == '1' && value == 2}
	raise "#{__method__} error" if s != 0
	s ={'1' => 1, '2' => 2,'3' => 3}.p2count{|key,value| value  > 1}
	raise "#{__method__} error" if s != 2
	s ={'1' => 1, '2' => 2,'3' => 3}.p2count
	raise "#{__method__} error" if s != 3
	s = [1,3,5,6,9].p2count{|e| e % 3 == 0}
	raise "#{__method__} error" if s != 3
	s = [1,3,5,6,9].p2count{|e| e > 0}
	raise "#{__method__} error" if s != 5
	s = [1,3,5,6,9].p2count
	raise "#{__method__} error" if s != 5
	p "#{__method__} passed"
end
#6
def test_p2cycle
	s = {'1' => 1, '2' => 2,'3' => 3}.p2cycle(2){|key,value| key + "test"}
	raise "#{__method__} error" if s != nil
	s = {'1' => 1, '2' => 2,'3' => 3}.p2cycle(-1){|key,value| key + "test"}
	raise "#{__method__} error" if s != nil
	s = [1,2,3,4].p2cycle(-1){|e| e + 1}
	raise "#{__method__} error" if s != nil
	s = [1,2,3,4].p2cycle(3){|e| e + 1}
	raise "#{__method__} error" if s != nil
	p "#{__method__} passed"
end
#7
def test_p2detect
	s = [2,3,4,6].p2detect{|e| e == 7}
	raise "#{__method__} error" if s != nil
	s = [2,3,4,6].p2detect{|e| e % 3 == 0}
	raise "#{__method__} error" if s != 3
	s = [].p2detect{|e| e == nil}
	raise "#{__method__} error" if s != nil
	s = {1=>2,3=>4,4=>5}.p2detect{|key,value| value == 2}
	raise "#{__method__} error" if s != [1,2]
	s = {1=>2,3=>4,4=>5}.p2detect{|key,value| value == 4}
	raise "#{__method__} error" if s != [3,4]
	p "#{__method__} passed"
end
#8
def test_p2drop
	s = {1=>2,3=>4,4=>5,5=>6}.p2drop(2)
	raise "#{__method__} error" if s != [[4,5],[5,6]]
	s = {1=>2,3=>4,4=>5,5=>6}.p2drop(5)
	raise "#{__method__} error" if s != []
	s = {1=>2,3=>4,4=>5,5=>6}.p2drop(0)
	raise "#{__method__} error" if s != [[1, 2], [3, 4], [4, 5], [5, 6]]
	s = [1 , 2 ,4 ,5 ,6 ,6].p2drop(5)
	raise "#{__method__} error" if s != [6]
	s = [1 , 2 ,4 ,5 ,6 ,6].p2drop(8)
	raise "#{__method__} error" if s != []
	s = [1 , 2 ,4 ,5 ,6 ,6].p2drop(0)
	raise "#{__method__} error" if s != [1, 2, 4, 5, 6, 6]
	p "#{__method__} passed"
end
#9
def test_p2drop_while
	s = [1 , 3 ,4 ,5 ,6 ,6].p2drop_while{|e| e % 2 == 1 }
	raise "#{__method__} error" if s != [4, 5, 6, 6]
	s = [1 , 3 ,4 ,5 ,6 ,6].p2drop_while{|e| e % 3 == 0 }
	raise "#{__method__} error" if s != [1, 3, 4, 5, 6, 6]
	s = [1 , 3 ,4 ,5 ,6 ,6].p2drop_while{|e| e < 10 }
	raise "#{__method__} error" if s != []
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2drop_while{|key,value| value % 10 == 0 }
	raise "#{__method__} error" if s != []
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2drop_while{|key,value| key != 3 || value != 30 }
	raise "#{__method__} error" if s != [[3, 30], [4, 40], [5, 50]]
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2drop_while{|key,value| key <= 3 }
	raise "#{__method__} error" if s != [[4, 40], [5, 50]]
	p "#{__method__} passed"
end
#10
def test_p2each_cons
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_cons(7) {|e| t << e}
	raise "#{__method__} error" if t != []
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_cons(2) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 3], [3, 4], [4, 5], [5, 6], [6, 6]]
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_cons(3) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 6]]
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_cons(1) {|e| t << e}
	raise "#{__method__} error" if t != [[1], [3], [4], [5], [6], [6]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_cons(2) {|e| t << e}
	raise "#{__method__} error" if t != [[[1, 10], [2, 20]], [[2, 20], [3, 30]], [[3, 30], [4, 40]], [[4, 40], [5, 50]]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_cons(4) {|e| t << e}
	raise "#{__method__} error" if t != [[[1, 10], [2, 20], [3, 30], [4, 40]], [[2, 20], [3, 30], [4, 40], [5, 50]]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_cons(6) {|e| t << e}
	raise "#{__method__} error" if t != []
	p "#{__method__} passed"
end
#11
def test_p2each_slice
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_slice(7) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 3, 4, 5, 6, 6]]
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_slice(2) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 3], [4, 5], [6, 6]]
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_slice(3) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 3, 4], [5, 6, 6]]
	t = Array.new
	s = [1 , 3 ,4 ,5 ,6 ,6].p2each_slice(1) {|e| t << e}
	raise "#{__method__} error" if t != [[1], [3], [4], [5], [6], [6]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_slice(2) {|e| t << e}
	raise "#{__method__} error" if t != [[[1, 10], [2, 20]], [[3, 30], [4, 40]], [[5, 50]]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_slice(4) {|e| t << e}
	raise "#{__method__} error" if t != [[[1, 10], [2, 20], [3, 30], [4, 40]], [[5, 50]]]
	t = Array.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_slice(6) {|e| t << e}
	raise "#{__method__} error" if t != [[[1, 10], [2, 20], [3, 30], [4, 40], [5, 50]]]
	p "#{__method__} passed"
end
#12
def test_p2each_with_index
	hash = Hash.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_with_index{|key,value| hash[key] = value }
	raise "#{__method__} error" if hash != {[1, 10]=>0, [2, 20]=>1, [3, 30]=>2, [4, 40]=>3, [5, 50]=>4}
	hash = Hash.new
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2each_with_index{|key,value| hash[value] = value }
	raise "#{__method__} error" if hash != {0=>0, 1=>1, 2=>2, 3=>3, 4=>4}
	hash = Hash.new
	s = [2,4,6,8,10].p2each_with_index{|key,value| hash[value] = value }
	raise "#{__method__} error" if hash != {0=>0, 1=>1, 2=>2, 3=>3, 4=>4}
	hash = Hash.new
	s = [2,4,6,8,10].p2each_with_index{|key,value| hash[value] = key }
	raise "#{__method__} error" if hash != {0=>2, 1=>4, 2=>6, 3=>8, 4=>10}
	s = [2,4,6,8,10].p2each_with_index{|key,value| key = key - value }
	raise "#{__method__} error" if s != [2, 4, 6, 8, 10]
	p "#{__method__} passed"
end
#13
def test_p2entries
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2entries
	raise "#{__method__} error" if s != [["e1", 1], ["de2", 2], ["cba3", 3], ["ba4", 4], ["a5", 5]]
	s = {'e1'=>1,'de2'=>'2','cba3'=>3,'ba4'=>'4','a5'=>5}.p2entries
	raise "#{__method__} error" if s != [["e1", 1], ["de2", "2"], ["cba3", 3], ["ba4", "4"], ["a5", 5]]
	s = {}.p2entries
	raise "#{__method__} error" if s != []
	s = [1,2,3,4,5,6].p2entries
	raise "#{__method__} error" if s != [1, 2, 3, 4, 5, 6]
	s = [].p2entries
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end
#14
def test_p2find
	s = [2,3,4,6].p2find{|e| e == 7}
	raise "#{__method__} error" if s != nil
	s = [2,3,4,6].p2find{|e| e % 3 == 0}
	raise "#{__method__} error" if s != 3
	s = [].p2find{|e| e == nil}
	raise "#{__method__} error" if s != nil
	s = {1=>2,3=>4,4=>5}.p2find{|key,value| value == 2}
	raise "#{__method__} error" if s != [1,2]
	s = {1=>2,3=>4,4=>5}.p2find{|key,value| value == 4}
	raise "#{__method__} error" if s != [3,4]
	p "#{__method__} passed"
end
#15
def test_p2find_all
	s = [2,3,4,6].p2find_all{|e| e == 7}
	raise "#{__method__} error" if s != []
	s = [2,3,4,6].p2find_all{|e| e % 3 == 0}
	raise "#{__method__} error" if s != [3,6]
	s = [].p2find_all{|e| e % 3 == 0}
	raise "#{__method__} error" if s != []
	s = {1=>2,3=>4,4=>5,6=>2}.p2find_all{|key,value| value == 2}
	raise "#{__method__} error" if s != [[1, 2], [6, 2]]
	s = {1=>2,3=>4,4=>5,6=>2}.p2find_all{|key,value| key % 2 == 0}
	raise "#{__method__} error" if s != [[4, 5], [6, 2]]
	s = {1=>2,3=>4,4=>5,6=>2,8=>7}.p2find_all{|key,value| key % 2 == 0 and value % 2 == 1}
	raise "#{__method__} error" if s != [[4, 5], [8, 7]]
	p "#{__method__} passed"
end
#16
def test_p2find_index
	s = {1=>2,3=>4,4=>5,6=>2,8=>7}.p2find_index{|key,value| key % 2 == 0 and value % 2 == 1}
	raise "#{__method__} error" if s != 2
	s = {1=>2,3=>4,4=>5,6=>2,8=>7}.p2find_index{|key,value| key == 6}
	raise "#{__method__} error" if s != 3
	s = {1=>2,3=>4,4=>5,6=>2,8=>7}.p2find_index{|key,value| value % 2 == 1}
	raise "#{__method__} error" if s != 2
	s = {1=>2,3=>4,4=>5,6=>2,8=>7}.p2find_index{|key,value| value > 10}
	raise "#{__method__} error" if s != nil
	s = [2,3,4,5,6].p2find_index{|e| e % 2 == 1}
	raise "#{__method__} error" if s != 1
	s = [2,3,4,5,6].p2find_index{|e| e >= 10}
	raise "#{__method__} error" if s != nil
	p "#{__method__} passed"
end
#17
def test_p2first
	s = [2,3,4,5,6].p2first(3)
	raise "#{__method__} error" if s != [2, 3, 4]
	s = [2,3,4,5,6].p2first(0)
	raise "#{__method__} error" if s != []
	s = [2,3,4,5,6].p2first(10)
	raise "#{__method__} error" if s != [2, 3, 4, 5, 6]
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2first(3)
	raise "#{__method__} error" if s != [["test1", 1], ["test2", 2], ["test3", 3]]
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2first(0)
	raise "#{__method__} error" if s != []
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2first(10)
	raise "#{__method__} error" if s != [["test1", 1], ["test2", 2], ["test3", 3], ["test4", 4], ["test5", 5]]
	p "#{__method__} passed"
end
#18
def test_p2group_by
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2group_by{|key,value| key.length > 5}
	raise "#{__method__} error" if s != {false=>[["test1", 1], ["test3", 3], ["test5", 5]], true=>[["test20", 2], ["test40", 4]]}
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2group_by{|key,value| value % 2 == 0}
	raise "#{__method__} error" if s != {false=>[["test1", 1], ["test3", 3], ["test5", 5]], true=>[["test20", 2], ["test40", 4]]}
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2group_by{|key,value| value < 6}
	raise "#{__method__} error" if s != {true=>[["test1", 1], ["test20", 2], ["test3", 3], ["test40", 4], ["test5", 5]]}
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2group_by{|key,value| value > 6}
	raise "#{__method__} error" if s != {false=>[["test1", 1], ["test20", 2], ["test3", 3], ["test40", 4], ["test5", 5]]}
	s = [10,20,30,-1,3,6].p2group_by{|e| e > 0}
	raise "#{__method__} error" if s != {true=>[10, 20, 30, 3, 6], false=>[-1]}
	s = [10,20,30,-1,3,6].p2group_by{|e| e % 2 == 0}
	raise "#{__method__} error" if s != {true=>[10, 20, 30, 6], false=>[-1, 3]}
	s = [10,20,30,14,3,6].p2group_by{|e| e < 0}
	raise "#{__method__} error" if s != {false=>[10, 20, 30, 14, 3, 6]}
	s = [10,20,30,14,3,6].p2group_by{|e| e > 0}
	raise "#{__method__} error" if s != {true=>[10, 20, 30, 14, 3, 6]}
	p "#{__method__} passed"
end
#19 inject(initial) { |memo, obj| block } → obj % same as reduce
def test_p2inject
	s = [10,20,30,14,3,6].p2inject(1){|s,e| s = s * e}
	raise "#{__method__} error" if s != 1512000
	s = [10,20,30,14,3,6].p2inject(0){|s,e| s = s + e}
	raise "#{__method__} error" if s != 83
	s = [10,20,30,14,3,6].p2inject(0){|s,e| s = s + e * 2}
	raise "#{__method__} error" if s != 166
	s = {'1' => 10, '2' => 20, '3' => 30 , '4' => 40}.p2inject(Array.new){|s,item| s << item}
	raise "#{__method__} error" if s != [["1", 10], ["2", 20], ["3", 30], ["4", 40]]
	s = {'1' => 10, '2' => 20, '3' => 30 , '4' => 40 , 'test' => 'test'}.p2inject(Array.new){|s,item| s << item}
	raise "#{__method__} error" if s != [["1", 10], ["2", 20], ["3", 30], ["4", 40], ["test", "test"]]
	p "#{__method__} passed"
end
#20 inject { |memo, obj| block } → obj
def test_p2para0inject
	s = [10,20,30,14,3,6].p2para0inject{|s,e| s = s * e}
	raise "#{__method__} error" if s != 1512000
	s = [10,20,30,14,3,6].p2para0inject{|s,e| s = s + e}
	raise "#{__method__} error" if s != 83
	s = [10,20,30,14,3,6].p2para0inject{|s,e| s = s + e * 2}
	raise "#{__method__} error" if s != 156
	s = {'1' => 10, '2' => 20, '3' => 30 , '4' => 40}.p2para0inject{|s,item| s << item}
	raise "#{__method__} error" if s != ["1", 10, ["2", 20], ["3", 30], ["4", 40]]
	s = {'1' => 10, '2' => 20, '3' => 30 , '4' => 40 , 'test' => 'test'}.p2para0inject{|s,item| s << item}
	raise "#{__method__} error" if s != ["1", 10, ["2", 20], ["3", 30], ["4", 40], ["test", "test"]]
	p "#{__method__} passed"
end
#21
def test_p2minmax
	s = ["cat","test","a","minmax","test_minmax"].p2minmax{|a ,b| a.length <=> b.length}
	raise "#{__method__} error" if s != ["a", "test_minmax"]
	s = [2,3,0,10,-4,7].p2minmax{|a ,b| a <=> b}
	raise "#{__method__} error" if s != [-4, 10]
	s = [2,3,0,10,-4,7].p2minmax{|a ,b| b <=> a}
	raise "#{__method__} error" if s != [10, -4]
	s = {"t1"=>1,"tx2"=>2,"zlzz"=>3,"xaxa"=>0,"daxxc"=>10,"a"=>-4,"test"=>7}.p2minmax{|a ,b| a <=> b}
	raise "#{__method__} error" if s != [["a", -4], ["zlzz", 3]]
	p "#{__method__} passed"
end
#22
def test_p2minmax_by
	s = {"t1"=>1,"tx2"=>2,"zlzz"=>3,"xaxa"=>0,"daxxc"=>10,"a"=>-4,"test"=>7}.p2minmax_by{|key ,value| key}
	raise "#{__method__} error" if s != [["a", -4], ["zlzz", 3]]
	s = {"t1"=>1,"tx2"=>2,"zlzz"=>3,"xaxa"=>0,"daxxc"=>10,"a"=>-4,"test"=>7}.p2minmax_by{|key ,value| key.length}
	raise "#{__method__} error" if s != [["a", -4], ["daxxc", 10]]
	s = {"t1"=>1,"tx2"=>2,"zlzz"=>30,"xaxa"=>0,"daxxc"=>10,"a"=>-4,"test"=>7}.p2minmax_by{|key ,value| value}
	raise "#{__method__} error" if s != [["a", -4], ["zlzz", 30]]
	s = {"t1"=>1,"tx2"=>2,"zlzz"=>30,"xaxa"=>0,"daxxc"=>10,"a"=>-4,"test"=>7}.p2minmax_by{|key ,value| -value}
	raise "#{__method__} error" if s != [["zlzz", 30], ["a", -4]]
	s = {}.p2minmax_by{|e| e * e}
	raise "#{__method__} error" if s != [nil, nil]
	s = [1 , 22, 33, 51, -6, 10, 14].p2minmax_by{|e| e}
	raise "#{__method__} error" if s != [-6, 51]
	s = [1 , 22, 33, 51, -6, 10, 14].p2minmax_by{|e| e * e}
	raise "#{__method__} error" if s != [1, 51]
	s = [].p2minmax_by{|e| e * e}
	raise "#{__method__} error" if s != [nil, nil]
	p "#{__method__} passed"
end

#23
def test_p2partition
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2partition{|key,value| key.length > 5}
	raise "#{__method__} error" if s != [[["test20", 2], ["test40", 4]], [["test1", 1], ["test3", 3], ["test5", 5]]]
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2partition{|key,value| value % 2 == 0}
	raise "#{__method__} error" if s != [[["test20", 2], ["test40", 4]], [["test1", 1], ["test3", 3], ["test5", 5]]]
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2partition{|key,value| value < 6}
	raise "#{__method__} error" if s != [[["test1", 1], ["test20", 2], ["test3", 3], ["test40", 4], ["test5", 5]], []]
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2partition{|key,value| value > 6}
	raise "#{__method__} error" if s != [[], [["test1", 1], ["test20", 2], ["test3", 3], ["test40", 4], ["test5", 5]]]
	s = [10,20,30,-1,3,6].p2partition{|e| e > 0}
	raise "#{__method__} error" if s != [[10, 20, 30, 3, 6], [-1]]
	s = [10,20,30,-1,3,6].p2partition{|e| e % 2 == 0}
	raise "#{__method__} error" if s != [[10, 20, 30, 6], [-1, 3]]
	s = [10,20,30,14,3,6].p2partition{|e| e < 0}
	raise "#{__method__} error" if s != [[], [10, 20, 30, 14, 3, 6]]
	s = [10,20,30,14,3,6].p2partition{|e| e > 0}
	raise "#{__method__} error" if s != [[10, 20, 30, 14, 3, 6], []]
	p "#{__method__} passed"
end
#24
def test_p2reject
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2reject{|key,value| key.length > 5}
	raise "#{__method__} error" if s != [["test1", 1], ["test3", 3], ["test5", 5]]
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2reject{|key,value| value % 2 == 0}
	raise "#{__method__} error" if s != [["test1", 1], ["test3", 3], ["test5", 5]]
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2reject{|key,value| value < 6}
	raise "#{__method__} error" if s != []
	s = {'test1'=>1,'test20'=>2,'test3'=>3,'test40'=>4,'test5'=>5}.p2reject{|key,value| value > 6}
	raise "#{__method__} error" if s != [["test1", 1], ["test20", 2], ["test3", 3], ["test40", 4], ["test5", 5]]
	s = [10,20,30,-1,3,6].p2reject{|e| e > 0}
	raise "#{__method__} error" if s != [-1]
	s = [10,20,30,-1,3,6].p2reject{|e| e % 2 == 0}
	raise "#{__method__} error" if s != [-1, 3]
	s = [10,20,30,14,3,6].p2reject{|e| e < 0}
	raise "#{__method__} error" if s != [10, 20, 30, 14, 3, 6]
	s = [10,20,30,14,3,6].p2reject{|e| e > 0}
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"	
end
#25
def test_p2take
	s = [2,3,4,5,6].p2take(3)
	raise "#{__method__} error" if s != [2, 3, 4]
	s = [2,3,4,5,6].p2take(0)
	raise "#{__method__} error" if s != []
	s = [2,3,4,5,6].p2take(10)
	raise "#{__method__} error" if s != [2, 3, 4, 5, 6]
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2take(3)
	raise "#{__method__} error" if s != [["test1", 1], ["test2", 2], ["test3", 3]]
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2take(0)
	raise "#{__method__} error" if s != []
	s = {'test1'=>1,'test2'=>2,'test3'=>3,'test4'=>4,'test5'=>5}.p2take(10)
	raise "#{__method__} error" if s != [["test1", 1], ["test2", 2], ["test3", 3], ["test4", 4], ["test5", 5]]
	p "#{__method__} passed"
end
#26
def test_p2take_while
	s = [2,3,4,5,6].p2take_while{|e| e < 4}
	raise "#{__method__} error" if s != [2, 3]
	s = [2,3,4,5,6].p2take_while{|e| e < 1}
	raise "#{__method__} error" if s != []
	s = [2,3,4,5,6].p2take_while{|e| e < 7}
	raise "#{__method__} error" if s != [2, 3, 4, 5, 6]
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2take_while{|key,value| value < 4}
	raise "#{__method__} error" if s != [["e1", 1], ["de2", 2], ["cba3", 3]]
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2take_while{|key,value| key > 'd'}
	raise "#{__method__} error" if s != [["e1", 1], ["de2", 2]]
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2take_while{|key,value| value >6}
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end
#27
def test_p2to_a
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2to_a
	raise "#{__method__} error" if s != [["e1", 1], ["de2", 2], ["cba3", 3], ["ba4", 4], ["a5", 5]]
	s = {'e1'=>1,'de2'=>'2','cba3'=>3,'ba4'=>'4','a5'=>5}.p2to_a
	raise "#{__method__} error" if s != [["e1", 1], ["de2", "2"], ["cba3", 3], ["ba4", "4"], ["a5", 5]]
	s = {}.p2to_a
	raise "#{__method__} error" if s != []
	s = [1,2,3,4,5,6].p2to_a
	raise "#{__method__} error" if s != [1, 2, 3, 4, 5, 6]
	s = [].p2to_a
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end
#28
def test_p2to_h
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2to_h
	raise "#{__method__} error" if s != {"e1"=>1, "de2"=>2, "cba3"=>3, "ba4"=>4, "a5"=>5}
	s = {'e1'=>1,'de2'=>'2','cba3'=>3,'ba4'=>'4','a5'=>5}.p2to_h
	raise "#{__method__} error" if s != {"e1"=>1, "de2"=>"2", "cba3"=>3, "ba4"=>"4", "a5"=>5}
	s = {}.p2to_h
	raise "#{__method__} error" if s != {}
	s = [[1,2],[3,4],[5,6]].p2to_h
	raise "#{__method__} error" if s != {1=>2, 3=>4, 5=>6}
	s = [].p2to_h
	raise "#{__method__} error" if s != {}
	p "#{__method__} passed"
end
#29 min {| a,b | block } → obj
def test_p2para0min
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2para0min{|a,b| a <=> b}
	raise "#{__method__} error" if s != ["a5", 5]
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2para0min{|a,b| a <=> b}
	raise "#{__method__} error" if s != [1, 10]
	s = [1,2,3,4,4,2,0].p2para0min{|a,b| a <=> b}
	raise "#{__method__} error" if s != 0
	s = [1,2,3,4,4,2,0].p2para0min{|a,b| b <=> a}
	raise "#{__method__} error" if s != 4
	s = [].p2para0min{|a,b| a <=> b}
	raise "#{__method__} error" if s != nil
	p "#{__method__} passed"
end
#30 max { |a, b| block } → obj
def test_p2para0max
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2para0max{|a,b| a <=> b}
	raise "#{__method__} error" if s != ["e1", 1]
	s = {1=>10,2=>20,3=>30,4=>40,5=>50}.p2para0max{|a,b| a <=> b}
	raise "#{__method__} error" if s != [5, 50]
	s = [1,2,3,4,4,2,0].p2para0max{|a,b| a <=> b}
	raise "#{__method__} error" if s != 4
	s = [1,2,3,4,4,2,0].p2para0max{|a,b| b <=> a}
	raise "#{__method__} error" if s != 0
	s = [].p2para0max{|a,b| a <=> b}
	raise "#{__method__} error" if s != nil
	p "#{__method__} passed"
end
#31 test_one?
def test_one?
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2one?{|key,value| key.length > 3}
	raise "#{__method__} error" if s != true
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2one?{|key,value| key.length > 2}
	raise "#{__method__} error" if s != false
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2one?{|key,value| value > 3}
	raise "#{__method__} error" if s != false
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2one?{|key,value| value == 3}
	raise "#{__method__} error" if s != true
	s = [3,4,2,0,11,-2,23,9,10].p2one?{|e| e < 0}
	raise "#{__method__} error" if s != true
	s = [3,4,2,0,11,-2,23,9,10].p2one?{|e| e > 0}
	raise "#{__method__} error" if s != false
	p "#{__method__} passed"
end
#32
def test_none?
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2none?{|key,value| key.length > 5}
	raise "#{__method__} error" if s != true
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2none?{|key,value| key.length > 2}
	raise "#{__method__} error" if s != false
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2none?{|key,value| value > 3}
	raise "#{__method__} error" if s != false
	s = {'e1'=>1,'de2'=>2,'cba3'=>3,'ba4'=>4,'a5'=>5}.p2none?{|key,value| value == 6}
	raise "#{__method__} error" if s != true
	s = [3,4,2,0,11,-2,23,9,10].p2none?{|e| e < 0}
	raise "#{__method__} error" if s != false
	s = [3,4,2,0,11,-2,23,9,10].p2none?{|e| e > 0}
	raise "#{__method__} error" if s != false
	p "#{__method__} passed"
end
#Test_all
def test_all
	test_p2all?
	test_p2any?
	test_p2collect
	test_p2collect_concat
	test_p2count
	test_p2cycle
	test_p2detect
	test_p2drop
	test_p2drop_while
	test_p2each_cons
	test_p2each_slice
	test_p2each_with_index
	test_p2entries
	test_p2find
	test_p2find_all
	test_p2find_index
	test_p2first
	test_p2group_by
	test_p2inject
	test_p2para0inject
	test_p2minmax
	test_p2minmax_by
	test_p2partition
	test_p2reject
	test_p2take
	test_p2take_while
	test_p2to_a
	test_p2to_h
end
test_all