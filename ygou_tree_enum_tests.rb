load 'ygou_tree.rb'
load 'ygou_enum.rb'

class P2Tree
	include P2Enumerable
	include Enumerable
	alias each p2each
end

#test1 :1 2 3 4 nil nil 5 6 7
test1_leaf1 = P2Tree.new(6)
test1_leaf2 = P2Tree.new(7)
test1_leaf3 = P2Tree.new(5)
test1_brach3 = P2Tree.new(4,test1_leaf1,test1_leaf2)
test1_brach2_1 = P2Tree.new(2,test1_brach3)
test1_brach2_2 = P2Tree.new(3,nil,test1_leaf3)
@test1_root = P2Tree.new(1,test1_brach2_1,test1_brach2_2)

#test2 : 1 2 3 4 5 6 7
test2_leaf1 = P2Tree.new(4)
test2_leaf2 = P2Tree.new(5)
test2_leaf3 = P2Tree.new(6)
test2_leaf4 = P2Tree.new(7)
test2_brach1 = P2Tree.new(2,test2_leaf1,test2_leaf2)
test2_brach2 = P2Tree.new(3,test2_leaf3,test2_leaf4)
@test2_root = P2Tree.new(1,test2_brach1,test2_brach2)

#test3 : 1 2 nil 3 nil 4 nil
test3_leaf = P2Tree.new("4")
test3_brach2 = P2Tree.new("3",test3_leaf)
test3_bracn1 = P2Tree.new("2",test3_brach2)
@test3_root = P2Tree.new("1",test3_bracn1)

#test4 : 1 nil 2 nil 3 nil 4
test4_leaf = P2Tree.new(4)
test4_brach2 = P2Tree.new(3,nil,test4_leaf)
test4_bracn1 = P2Tree.new(2,nil,test4_brach2)
@test4_root = P2Tree.new(1,nil,test4_bracn1)

#1
def test_tree_p2all?
	s = @test1_root.p2all?{|e| e % 2 == 0}
	raise "#{__method__} error" if s != false
	s = @test2_root.p2all?
	raise "#{__method__} error" if s != true
	s = @test3_root.p2all?{|e| e.length < 5}
	raise "#{__method__} error" if s != true
	s = @test4_root.p2all?{|e| e > 2 }
	raise "#{__method__} error" if s != false
	p "#{__method__} passed"
end
#2
def test_tree_p2any?
	s = @test1_root.p2any?{|e| e % 2 == 0}
	raise "#{__method__} error" if s != true
	s = @test2_root.p2any?
	raise "#{__method__} error" if s != true
	s = @test4_root.p2any?{|e| e > 5}
	raise "#{__method__} error" if s != false
	s = @test3_root.p2any?{|e| e == '5'}
	raise "#{__method__} error" if s != false
	p "#{__method__} passed"
end
#3
def test_tree_p2collect
	s = @test1_root.p2collect{|e| e  - 1}
	raise "#{__method__} error" if s != [0, 1, 3, 5, 6, 2, 4]
	s = @test2_root.p2collect{|e| e  * e}
	raise "#{__method__} error" if s != [1, 4, 16, 25, 9, 36, 49]
	s = @test3_root.p2collect{|e| e  + "test"}
	raise "#{__method__} error" if s != ["1test", "2test", "3test", "4test"]
	s = @test4_root.p2collect{|e| [e,-e] }
	raise "#{__method__} error" if s != [[1, -1], [2, -2], [3, -3], [4, -4]]
	p "#{__method__} passed"
end

#4
def test_tree_p2collect_concat
	s = @test1_root.p2collect_concat{|e| [e,-e]}
	raise "#{__method__} error" if s != [1, -1, 2, -2, 4, -4, 6, -6, 7, -7, 3, -3, 5, -5]
	s = @test2_root.p2collect_concat{|e| e + 1}
	raise "#{__method__} error" if s != [2, 3, 5, 6, 4, 7, 8]
	s = @test3_root.p2collect_concat{|e| e + "test"}
	raise "#{__method__} error" if s != ["1test", "2test", "3test", "4test"]
	s = @test4_root.p2collect_concat{|e| e}
	raise "#{__method__} error" if s != [1,2,3,4]
	p "#{__method__} passed"
end

#5
def test_tree_p2count
	s = @test1_root.p2count{|e| e % 2 == 1 }
	raise "#{__method__} error" if s != 4
	s = @test1_root.p2count{|e| e % 2 == 0 }
	raise "#{__method__} error" if s != 3
	s = @test1_root.p2count{|e| e > 7 }
	raise "#{__method__} error" if s != 0
	s = @test2_root.p2count{|e| e == 5 }
	raise "#{__method__} error" if s != 1
	s = @test3_root.p2count{|e| e .length == 1 }
	raise "#{__method__} error" if s != 4
	s = @test4_root.p2count{|e| e % 2 == 0 }
	raise "#{__method__} error" if s != 2
	p "#{__method__} passed"
end

#6
def test_tree_p2cycle
	s = @test1_root.p2cycle(1){|e| e + 1 }
	raise "#{__method__} error" if s != nil
	test = Array.new
	s = @test1_root.p2cycle(2){|e| test << e }
	raise "#{__method__} error" if test != [1, 2, 4, 6, 7, 3, 5, 1, 2, 4, 6, 7, 3, 5]
	test = Array.new
	s = @test2_root.p2cycle(2){|e| test << e }
	raise "#{__method__} error" if test != [1, 2, 4, 5, 3, 6, 7, 1, 2, 4, 5, 3, 6, 7]
	test = Array.new
	s = @test3_root.p2cycle(2){|e| test << e }
	raise "#{__method__} error" if test != ["1", "2", "3", "4", "1", "2", "3", "4"]
	test = Array.new
	s = @test4_root.p2cycle(0){|e| test << e }
	raise "#{__method__} error" if test != []
	p "#{__method__} passed"
end
#7

def test_tree_p2detect
	s = @test1_root.p2detect(nil){|e| e > 3 }
	raise "#{__method__} error" if s != 4
	s = @test1_root.p2detect(nil){|e| e == 3 }
	raise "#{__method__} error" if s != 3
	s = @test2_root.p2detect(nil){|e| e == 8 }
	raise "#{__method__} error" if s != nil
	s = @test3_root.p2detect(nil){|e| e.length == 1 }
	raise "#{__method__} error" if s != "1"
	s = @test4_root.p2detect{|e| e % 2== 0 }
	raise "#{__method__} error" if s != 2
	p "#{__method__} passed"
end

#8
def test_tree_p2drop
	s = @test1_root.p2drop(3)
	raise "#{__method__} error" if s != [6, 7, 3, 5]
	s = @test2_root.p2drop(4)
	raise "#{__method__} error" if s != [3, 6, 7]
	s = @test3_root.p2drop(3)
	raise "#{__method__} error" if s != ["4"]
	s = @test4_root.p2drop(5)
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end

#9
def test_tree_p2drop_while
	s = @test1_root.p2drop_while{|e| e < 3}
	raise "#{__method__} error" if s != [4, 6, 7, 3, 5]
	s = @test2_root.p2drop_while{|e| e > 3}
	raise "#{__method__} error" if s != [1, 2, 4, 5, 3, 6, 7]
	s = @test3_root.p2drop_while{|e| e.length == 1}
	raise "#{__method__} error" if s != []
	s = @test4_root.p2drop_while{|e| e % 2 == 1}
	raise "#{__method__} error" if s != [2, 3, 4]
	p "#{__method__} passed"
end

#10

def test_tree_p2each_cons
	t = Array.new
	s = @test1_root.p2each_cons(2) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 2], [2, 4], [4, 6], [6, 7], [7, 3], [3, 5]]
	t = Array.new
	s = @test2_root.p2each_cons(3) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 2, 4], [2, 4, 5], [4, 5, 3], [5, 3, 6], [3, 6, 7]]
	t = Array.new
	s = @test3_root.p2each_cons(1) {|e| t << e}
	raise "#{__method__} error" if t != [["1"], ["2"], ["3"], ["4"]]
	t = Array.new
	s = @test4_root.p2each_cons(5) {|e| t << e}
	raise "#{__method__} error" if t != []
	p "#{__method__} passed"
end

#11
def test_tree_p2each_slice
	t = Array.new
	s = @test1_root.p2each_slice(1) {|e| t << e}
	raise "#{__method__} error" if t != [[1], [2], [4], [6], [7], [3], [5]]
	t = Array.new
	s = @test2_root.p2each_slice(3) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 2, 4], [5, 3, 6], [7]]
	t = Array.new
	s = @test3_root.p2each_slice(2) {|e| t << e}
	raise "#{__method__} error" if t != [["1", "2"], ["3", "4"]]
	t = Array.new
	s = @test4_root.p2each_slice(5) {|e| t << e}
	raise "#{__method__} error" if t != [[1, 2, 3, 4]]
	p "#{__method__} passed"
end
#12
def test_tree_p2each_with_index
	t = Hash.new
	s = @test1_root.p2each_with_index{|item,index| t[item] = index}
	raise "#{__method__} error" if t != {1=>0, 2=>1, 4=>2, 6=>3, 7=>4, 3=>5, 5=>6}
	t = Hash.new
	s = @test2_root.p2each_with_index{|item,index| t[index] = item}
	raise "#{__method__} error" if t != {0=>1, 1=>2, 2=>4, 3=>5, 4=>3, 5=>6, 6=>7}
	t = Hash.new
	s = @test3_root.p2each_with_index{|item,index| t[item] = index + 1}
	raise "#{__method__} error" if t != {"1"=>1, "2"=>2, "3"=>3, "4"=>4}
	t = Hash.new
	s = @test4_root.p2each_with_index{|item,index| t[item] = index % 2 == 0}
	raise "#{__method__} error" if t != {1=>true, 2=>false, 3=>true, 4=>false}
	p "#{__method__} passed"
end
#13
def test_tree_p2entries
	s = @test1_root.p2entries
	raise "#{__method__} error" if s != [1, 2, 4, 6, 7, 3, 5]
	s = @test2_root.p2entries
	raise "#{__method__} error" if s != [1, 2, 4, 5, 3, 6, 7]
	s = @test3_root.p2entries
	raise "#{__method__} error" if s != ["1", "2", "3", "4"]
	s = @test4_root.p2entries
	raise "#{__method__} error" if s != [1, 2, 3, 4]
	p "#{__method__} passed"
end
#14
def test_tree_p2find
	s = @test1_root.p2find(nil){|e| e > 3 }
	raise "#{__method__} error" if s != 4
	s = @test1_root.p2find(nil){|e| e == 3 }
	raise "#{__method__} error" if s != 3
	s = @test2_root.p2find(nil){|e| e == 8 }
	raise "#{__method__} error" if s != nil
	s = @test3_root.p2find(nil){|e| e.length == 1 }
	raise "#{__method__} error" if s != "1"
	s = @test4_root.p2find{|e| e % 2== 0 }
	raise "#{__method__} error" if s != 2
	p "#{__method__} passed"
end
#15
def test_tree_p2find_all
	s = @test1_root.p2find_all{|e| e % 2 == 0}
	raise "#{__method__} error" if s != [2, 4, 6]
	s = @test2_root.p2find_all{|e| e != 3}
	raise "#{__method__} error" if s != [1, 2, 4, 5, 6, 7]
	s = @test3_root.p2find_all{|e| e.length < 2}
	raise "#{__method__} error" if s != ["1", "2", "3", "4"]
	s = @test4_root.p2find_all{|e| e > 2}
	raise "#{__method__} error" if s != [3, 4]
	s = @test4_root.p2find_all{|e| e > 5}
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end

#16
def test_tree_p2find_index
	s = @test1_root.p2find_index{|e| e % 2 == 0}
	raise "#{__method__} error" if s != 1
	s = @test2_root.p2find_index{|e| e != 3}
	raise "#{__method__} error" if s != 0
	s = @test3_root.p2find_index{|e| e.length < 2}
	raise "#{__method__} error" if s != 0
	s = @test4_root.p2find_index{|e| e > 2}
	raise "#{__method__} error" if s != 2
	s = @test4_root.p2find_index{|e| e > 5}
	raise "#{__method__} error" if s != nil
	p "#{__method__} passed"
end

#17
def test_tree_p2first
	s = @test1_root.p2first(3)
	raise "#{__method__} error" if s != [1, 2, 4]
	s = @test2_root.p2first(8)
	raise "#{__method__} error" if s != [1, 2, 4, 5, 3, 6, 7]
	s = @test3_root.p2first(0)
	raise "#{__method__} error" if s != []
	s = @test4_root.p2first(2)
	raise "#{__method__} error" if s != [1,2]
	p "#{__method__} passed"
end
#18
def test_tree_p2group_by
	s = @test1_root.p2group_by{|e| e % 3}
	raise "#{__method__} error" if s != {1=>[1, 4, 7], 2=>[2, 5], 0=>[6, 3]}
	s = @test2_root.p2group_by{|e| e % 2}
	raise "#{__method__} error" if s != {1=>[1, 5, 3, 7], 0=>[2, 4, 6]}
	s = @test3_root.p2group_by{|e| e.length}
	raise "#{__method__} error" if s != {1=>["1", "2", "3", "4"]}
	s = @test4_root.p2group_by{|e| e}
	raise "#{__method__} error" if s != {1=>[1], 2=>[2], 3=>[3], 4=>[4]}
	p "#{__method__} passed"
end
#19 inject(initial) { |memo, obj| block } → obj % same as reduce
def test_tree_p2inject
	s = @test1_root.p2inject(100){|s ,e| s = s + e}
	raise "#{__method__} error" if s != 128
	s = @test2_root.p2inject(-1){|s ,e| s = s * e}
	raise "#{__method__} error" if s != -5040
	s = @test3_root.p2inject("test"){|s ,e| s = s + e}
	raise "#{__method__} error" if s != "test1234"
	s = @test4_root.p2inject(0){|s ,e| s = s / e}
	raise "#{__method__} error" if s != 0
	p "#{__method__} passed"
end
#20 inject { |memo, obj| block } → obj
def test_tree_p2para0inject
	s = @test1_root.p2para0inject{|s ,e| s = s + e}
	raise "#{__method__} error" if s != 28
	s = @test2_root.p2para0inject{|s ,e| s = s * e}
	raise "#{__method__} error" if s != 5040
	s = @test3_root.p2para0inject{|s ,e| s = s + e}
	raise "#{__method__} error" if s != "1234"
	s = @test4_root.p2para0inject{|s ,e| s = s / e}
	raise "#{__method__} error" if s != 0
	p "#{__method__} passed"
end
#21 
def test_tree_p2minmax
	s = @test1_root.p2minmax{|a,b| a<=>b }
	raise "#{__method__} error" if s != [1, 7]
	s = @test2_root.p2minmax{|a,b| b<=>a }
	raise "#{__method__} error" if s != [7, 1]
	s = @test3_root.p2minmax{|a,b| a.length<=>b.length}
	raise "#{__method__} error" if s != ["1", "1"]
	s = @test4_root.p2minmax{|a,b| a<=>b}
	raise "#{__method__} error" if s != [1, 4]
	p "#{__method__} passed"
end
#22
def test_tree_p2minmax_by
	s = @test1_root.p2minmax_by{|e| e}
	raise "#{__method__} error" if s != [1, 7]
	s = @test2_root.p2minmax_by{|e| -e}
	raise "#{__method__} error" if s != [7, 1]
	s = @test3_root.p2minmax_by{|e| e.length}
	raise "#{__method__} error" if s != ["1", "1"]
	s = @test4_root.p2minmax_by{|e| e * e}
	raise "#{__method__} error" if s != [1, 4]
	p "#{__method__} passed"
end
#23
def test_tree_p2partition
	s = @test1_root.p2partition{|e| e > 3}
	raise "#{__method__} error" if s != [[4, 6, 7, 5], [1, 2, 3]]
	s = @test2_root.p2partition{|e| e % 2 == 0}
	raise "#{__method__} error" if s != [[2, 4, 6], [1, 5, 3, 7]]
	s = @test3_root.p2partition{|e| e.length > 2}
	raise "#{__method__} error" if s != [[], ["1", "2", "3", "4"]]
	s = @test4_root.p2partition{|e| e < 3}
	raise "#{__method__} error" if s != [[1, 2], [3, 4]]
	p "#{__method__} passed"
end
#24
def test_tree_p2reject
	s = @test1_root.p2reject{|e| e > 3}
	raise "#{__method__} error" if s != [1, 2, 3]
	s = @test2_root.p2reject{|e| e % 2 == 0}
	raise "#{__method__} error" if s != [1, 5, 3, 7]
	s = @test3_root.p2reject{|e| e.length > 2}
	raise "#{__method__} error" if s != ["1", "2", "3", "4"]
	s = @test4_root.p2reject{|e| e < 3}
	raise "#{__method__} error" if s != [3, 4]
	p "#{__method__} passed"
end

#25
def test_tree_p2take
	s = @test1_root.p2take(3)
	raise "#{__method__} error" if s != [1, 2, 4]
	s = @test2_root.p2take(8)
	raise "#{__method__} error" if s != [1, 2, 4, 5, 3, 6, 7]
	s = @test3_root.p2take(0)
	raise "#{__method__} error" if s != []
	s = @test4_root.p2take(2)
	raise "#{__method__} error" if s != [1,2]
	p "#{__method__} passed"
end

#26
def test_tree_take_while
	s = @test1_root.p2take_while{|e| e < 3}
	raise "#{__method__} error" if s != [1, 2]
	s = @test2_root.p2take_while{|e| e % 2 == 1}
	raise "#{__method__} error" if s != [1]
	s = @test3_root.p2take_while{|e| e.length < 2}
	raise "#{__method__} error" if s != ["1", "2", "3", "4"]
	s = @test4_root.p2take_while{|e| e > 3}
	raise "#{__method__} error" if s != []
	p "#{__method__} passed"
end

#27
def test_tree_to_a
	s = @test1_root.p2to_a
	raise "#{__method__} error" if s != [1, 2, 4, 6, 7, 3, 5]
	s = @test2_root.p2to_a
	raise "#{__method__} error" if s != [1, 2, 4, 5, 3, 6, 7]
	s = @test3_root.p2to_a
	raise "#{__method__} error" if s != ["1", "2", "3", "4"]
	s = @test4_root.p2to_a
	raise "#{__method__} error" if s != [1, 2, 3, 4]
	p "#{__method__} passed"
end


#28
def test_tree_to_h
	s = @test1_root.p2each_with_level_to_h
	raise "#{__method__} error" if s != {1=>1, 2=>2, 4=>3, 6=>4, 7=>4, 3=>2, 5=>3}
	s = @test2_root.p2each_with_level_to_h
	raise "#{__method__} error" if s != {1=>1, 2=>2, 4=>3, 5=>3, 3=>2, 6=>3, 7=>3}
	s = @test3_root.p2each_with_level_to_h
	raise "#{__method__} error" if s != {"1"=>1, "2"=>2, "3"=>3, "4"=>4}
	s = @test4_root.p2each_with_level_to_h
	raise "#{__method__} error" if s != {1=>1, 2=>2, 3=>3, 4=>4}
	p "#{__method__} passed"
end

def test_all
	test_tree_p2all?
	test_tree_p2any?
	test_tree_p2collect
	test_tree_p2collect_concat
	test_tree_p2count
	test_tree_p2cycle
	test_tree_p2detect
	test_tree_p2drop
	test_tree_p2drop_while
	test_tree_p2each_cons
	test_tree_p2each_slice
	test_tree_p2each_with_index
	test_tree_p2entries
	test_tree_p2find
	test_tree_p2find_all
	test_tree_p2find_index
	test_tree_p2first
	test_tree_p2group_by
	test_tree_p2inject
	test_tree_p2inject
	test_tree_p2minmax
	test_tree_p2minmax_by
	test_tree_p2partition
	test_tree_p2reject
	test_tree_p2take
	test_tree_take_while
	test_tree_to_a
	test_tree_to_h
end

#test_all
