1.Yupeng Gou for CSC 453

2.Download all my files into a same directory
  2.1 To test my P2Enumberable method, you shoud load my 'ygou_enum_tests.rb' file, I have added "load 'ygou_enum.rb'"at the beginning of this file, so you just need to make sure the 'ygou_enum.rb' file is just in the same directory with it. Then you can run 'test_all' method to test all methods at one time.
  2.2 To test my P2Tree Class, you can load 'ygou_tree_tests.rb' file,and also make sure the 'ygou_tree.rb' file is in the same directory. Then you can run 'test_Tree_p2each' function to test my p2each method, and run 'test_Tree_p2each_with_level' method to test my p2each_with_level method.
(I have already built four trees in this file ,and all the tests are based on these)
  2,3 To test my P2Tree and P2Enumerable integration, you should load 'ygou_tree_enum_tests' file and make sure the 'ygou_tree.rb' file and the 'ygou_enum.rb' file are in the same directory. Then you can run 'test_all' function to test all the P2Enumberable methods.(The optional methods are not excuted in 'test_all')

3.There is a p2each in each iterator , and most iteration stops when this p2each finishes , which means every element has been accessed and evaluated.Also ,there are some exceptions that the iteration may stop before the p2each method comes to the end.
Those exceptions are:
p2cycle method: if n == nil ,this iteration will never stop
		else if n <= 0 , it will stop immediately
		else the iteration will stop after the p2each comes to an end
p2count method: if block == nil , return self.size immediately
p2detect method: stop when an anymous funtion returns true
		 otherwise,stops after the end of p2each
p2find_index method: stop when an anymous funtion returns true
		 otherwise,stops after the end of p2each
p2take_while method: stop when an anymous funtion returns false
		 otherwise,stops after the end of p2each

All orther methods stop interation after the end of p2each

4. I haven't finished the C Unit test.But I have implemented some optional methods.