=begin
	
Unit test for 28 Enumerable methods implemented in Ruby anonymous function
	
=end


# require("./30114609_enum/P2Enumerable.rb")
require("./txie3_enum.rb")

class Array

	#import the P2Enumerable module so that Array can have the method in P2Enumerable module

	include P2Enumerable
	alias p2each each

end

class Range
	include P2Enumerable
	alias p2each each
end

class Hash
	include P2Enumerable
	alias p2each each
end

# used to print the detail of the test case

def printerror(methodName, expected, actual, classname)
	raise "#{methodName} for #{classname} error\nexpected #{expected} actual is #{actual}" if actual != expected
	puts "#{methodName} for #{classname} passed"
end

def test_p2all?
	p "#{__method__}"
	res = [].p2all? {|e| e.length <= 4 }
	printerror(__method__, [].all?, res, "empty array")
	res = ["China","Japan","USA"].p2all? {|e| e.length <= 4 }
	printerror(__method__, false, res, "array")
	# res = [].p2all?
	# printerror(__method__, [].all?, res, "empty array without block")
	# res = [nil,"Japan","USA"].p2all?
	# printerror(__method__, false, res, "array without block")

	res = {}.p2all?
	printerror(__method__, {}.all?, res, "empty hash without block")
	res = {}.p2all? {|e| e.length <= 4 }
	printerror(__method__, {}.all? {|e| e.length <= 4}, res, "empty hash")
	res = {"China"=>0,"Japan"=>1,"USA"=>2}.p2all?{|e| e[0].length <= 4 }
	printerror(__method__, false, res, "hash")
end

test_p2all?

def test_p2any?
	p "#{__method__}"
	res = [].p2any?{|e| e<= 30}
	printerror(__method__, [].any?{|e| e<= 30}, res, "empty array")
	res = [1, 20,34].p2any?{|e| e <= 30}
	printerror(__method__, true, res, "array")
	# res = [1, nil, nil].p2any?
	# printerror(__method__, true, res, "array without block")

	res = {}.p2any?{|e| e[0] <= 30}
	printerror(__method__, {}.any?{|e| e<= 30}, res, "empty hash")
	res = {1=>'a',2=>'b',3=>'c'}.p2any?{|e| e.length >1}
	printerror(__method__, {1=>'a',2=>'b',3=>'c'}.any?{|e| e.length >1}, res, "hash")
	# res = {}.p2any?
	# printerror(__method__, {}.any?{|e| e<= 30}, res, "empty hash without block")
	# res = {1=>'a',2=>'b',3=>nil}.p2any?
	# printerror(__method__, {1=>'a',2=>'b',3=>nil}.any?, res, "hash without block")
end

test_p2any?

def test_p2collect
	p "#{__method__}"
	res = [].p2collect{|e| e**3}
	printerror(__method__, [].collect{|e| e**3}, res, "empty array")
	res = [1,2,3].p2collect{|e| e**3}
	printerror(__method__, [1,8,27], res, "array")
	res = {}.p2collect{|e| e[0]**3}
	printerror(__method__, {}.collect{|e| e[0]**3}, res, "empty hash")
	res = {'a'=>1,'b'=>2,'c'=>3}.p2collect {|e| e[1]**3}
	printerror(__method__, {'a'=>1,'b'=>2,'c'=>3}.collect {|e| e[1]**3}, res, "hash")

end

test_p2collect

def test_p2collect_concat
	p "#{__method__}"
	res = [].p2collect_concat{|e| [e, e**2]}
	printerror(__method__, [].collect_concat{|e| [e, e**2]}, res, "empty array")
	res = [1,2,3].p2collect_concat{|e| [e, e**2]}
	printerror(__method__, [1,1,2,4,3,9], res, "array")
	res = {}.p2collect_concat{|e| [e, e[1]**2]}
	printerror(__method__, {}.collect_concat{|e| [e, e[1]**2]}, res, "empty hash")
	res = {'a'=>1,'b'=>2,'c'=>3}.p2collect_concat{|e| [e, e[1]**2]}
	printerror(__method__, [['a',1],1,['b',2],4,['c',3],9], res, "hash")
end

test_p2collect_concat

def test_p2count
	p "#{__method__}"
	res = [].p2count {|e| e%2 == 0}
	printerror(__method__, 0,res,"empty array")
	# res = [1,4,6,9,23].p2count
	# printerror(__method__,5,res,"array without block")
	res = [1,4,6,9,23].p2count {|e| e%2==0}
	printerror(__method__, 2,res,"array")

	res = {}.p2count {|e| e%2 == 1}
	printerror(__method__, 0, res, "empty hash")
	# res = {'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5}.p2count
	# printerror(__method__, 5, res, "hash without block")
	res = {'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5}.p2count{|e| e[1]%2==1}
	printerror(__method__, 3, res, "hash")

end

test_p2count

def test_p2cycle
	p "#{__method__}"
	res= []
	["C","H","N"].p2cycle(2) {|e| res<<(e+"!")}
	printerror(__method__, ["C!","H!","N!","C!","H!","N!"], res, "array")
	res = []
	{'a'=>1,'b'=>2,'c'=>3}.p2cycle(2) {|e| res << e[1]+2}
	printerror(__method__, [3,4,5,3,4,5], res, "hash")
end

# test_p2cycle

# def test_p2detect
# 	p "#{__method__}"
# 	res = [].p2detect(nil) {|e| e%2 == 0}
# 	printerror(__method__, nil, res, "empty array")
# 	res = (1..5).to_a.p2detect(nil) {|e| e%2 == 0}
# 	printerror(__method__, 2, res, "array")

# 	res = {}.p2detect(nil) {|e| e[1]%2 == 0}
# 	printerror(__method__, {}.detect(nil) {|e| e[1]%2 == 0}, res, "empty hash")
# end
def test_p2detect
	p "#{__method__}"
	res = [].p2detect {|e| e%2 == 0}
	printerror(__method__, nil, res, "empty array")
	res = (1..5).to_a.p2detect {|e| e%2 == 0}
	printerror(__method__, 2, res, "array")

	res = {}.p2detect {|e| e[1]%2 == 0}
	printerror(__method__, {}.detect(nil) {|e| e[1]%2 == 0}, res, "empty hash")
end

test_p2detect

def test_p2drop
	p "#{__method__}"
	res = [].p2drop(3)
	printerror(__method__,[].drop(3),res,"empty array")
	res = ["C","H","N","L","P","L"].p2drop(3)
	printerror(__method__,['L','P','L'],res,"array")

	res = {}.p2drop(3)
	printerror(__method__,[].drop(3),res,"empty hash")
	res = {'a'=>1,'b'=>2,'c'=>3}.p2drop(1)
	printerror(__method__,{'a'=>1,'b'=>2,'c'=>3}.drop(1),res,"hash")

end

test_p2drop

def test_p2drop_while
	p "#{__method__}"
	res = [].p2drop_while {|e| e.length<6}
	printerror(__method__,[].drop_while {|e| e.length<6},res,"empty array")
	res = ["CHINA","JAPAN","USA","RUSSIA","UK"].p2drop_while {|e| e.length<6}
	printerror(__method__,['RUSSIA','UK'],res,"array")

	res = {}.p2drop_while {|e| e.length<6}
	printerror(__method__,{}.drop_while {|e| e[1].length<6},res,"empty hash")
	res = {1=>"CHINA",2=>"JAPAN",3=>"USA",4=>"RUSSIA",5=>"UK"}.p2drop_while {|e| e[1].length<6}
	printerror(__method__,{1=>"CHINA",2=>"JAPAN",3=>"USA",4=>"RUSSIA",5=>"UK"}.drop_while {|e| e[1].length<6},res,"hash")

end

test_p2drop_while

def test_p2each_cons
	p "#{__method__}"
	sum = 0
	[].p2each_cons(2) do |e|
		sum = e.inject {|res,ele| res + ele}
	end
	printerror(__method__,0,sum,"empty array")

	sum = 0
	[1,2,10].p2each_cons(2) do |e|
		sum = e.inject {|res,ele| res + ele}
	end
	printerror(__method__,12,sum,"array")

	sum = 0
	{}.p2each_cons(2) do |e|
		sum = e.inject {|res,ele| res + ele[1]}
	end
	printerror(__method__,0,sum,"empty hash")

	sum = 0
	{1=>1,2=>2,3=>10}.p2each_cons(2) do |e|
		sum = e.inject(0) {|res, i| res + i[1]}
	end
	printerror(__method__,12,sum,"hash")
end

# test_p2each_cons

def test_p2each_slice
	p "#{__method__}"
	# temp = Array.new
	# [1,2,10].p2each_slice(2) {|e| temp << e.p2all? {|i| i<10}}
	# res = temp.all? {|i| i == true}
	# printerror(__method__, false, res, "array")

	  x = []
	  r = [1, 2, 3, 4].p2each_slice(3) { |e| x << e }
	  # raise "#{__method__} error" if x != [[1, 2, 3], [4]] || r != nil
	  # p "#{__method__} passed"
	  printerror(__method__, [[1, 2, 3], [4]], x, "array")
end

test_p2each_slice

def test_p2each_with_index
	p "#{__method__}"
	res = Array.new
	[1,2,5,9,13].p2each_with_index {|e, index| res << e - index  }
	printerror(__method__, [1,1,3,6,9],res,"array")
	res = Array.new
	{1=>1,2=>2,3=>10}.p2each_with_index {|e, index| res << e[1] - index  }
	printerror(__method__, [1,1,8],res,"hash")

end

test_p2each_with_index

def test_p2entries
	p "#{__method__}"
	res = (1..4).p2entries
	printerror(__method__,[1,2,3,4],res,"array")

	res = {1=>1,2=>2,3=>10}.p2entries
	printerror(__method__,{1=>1,2=>2,3=>10}.entries,res,"hash")
	
end

test_p2entries

def test_p2find
	p "#{__method__}"
	res = [].p2find(nil) {|e| e%2 == 0}
	printerror(__method__,[].find(nil){|e| e%2 == 0},res,"empty array")
	res = (1..5).to_a.p2find(nil) {|e| e%2 == 0}
	printerror(__method__,2,res,"array")

	res = {}.p2find(nil) {|e| e[1]%2 == 0}
	printerror(__method__,[].find(nil){|e| e[1]%2 == 0},res,"empty hash")
	res = {1=>1,2=>2,3=>0}.p2find(nil) {|e| e[1]%2 == 0}
	printerror(__method__,{1=>1,2=>2,3=>0}.find(nil) {|e| e[1]%2 == 0},res,"hash")
end

def test_p2find
	p "#{__method__}"
	res = [].p2find {|e| e%2 == 0}
	printerror(__method__,[].find {|e| e%2 == 0},res,"empty array")
	res = (1..5).to_a.p2find {|e| e%2 == 0}
	printerror(__method__,2,res,"array")

	res = {}.p2find {|e| e[1]%2 == 0}
	printerror(__method__,[].find {|e| e[1]%2 == 0},res,"empty hash")
	res = {1=>1,2=>2,3=>0}.p2find {|e| e[1]%2 == 0}
	printerror(__method__,{1=>1,2=>2,3=>0}.find {|e| e[1]%2 == 0},res,"hash")
end

test_p2find

def test_p2find_all
	p "#{__method__}" 
	res = [1,2,4,10].p2find_all {|e| e<=4}
	printerror(__method__,[1,2,4],res,"array")

	res = {1=>1,2=>2,3=>4,4=>10}.p2find_all {|e| e[1]%2 == 0}
	printerror(__method__,{1=>1,2=>2,3=>4,4=>10}.find_all {|e| e[1]%2 == 0},res,"hash")
end

test_p2find_all

def test_p2find_index
	p "#{__method__}"
	res = [1,2,4,10].p2find_index {|e| e>=4}
	printerror(__method__,2,res,"array")
	res = [1,2,4,10].p2find_index {|e| e>=11}
	printerror(__method__,nil,res,"array without match")

	res = {1=>1,2=>2,3=>4,4=>10}.p2find_index {|e| e[1]>=4}
	printerror(__method__,2,res,"hash")
	res = {1=>1,2=>2,3=>4,4=>10}.p2find_index {|e| e[1]>=11}
	printerror(__method__,nil,res,"hash without match")
end

test_p2find_index

def test_p2first 
	p "#{__method__}"
	res = [1,2,4,10].p2first(1)
	printerror(__method__,[1],res,"array")
	res = [].p2first(1)
	printerror(__method__,[],res,"array with empty collection")

	res = {1=>1,2=>2,3=>4,4=>10}.p2first(1)
	printerror(__method__,{1=>1,2=>2,3=>4,4=>10}.first(1),res,"hash")
	res = {}.p2first(1)
	printerror(__method__,[],res,"hash with empty collection")

	
end

test_p2first

def test_p2group_by 
	p "#{__method__}"
	res = [].p2group_by {|e| e%2}
	printerror(__method__,[].group_by {|e| e%2},res,"empty array")
	res = [1,2,3,4,5].p2group_by {|e| e%2}
	printerror(__method__,[1,2,3,4,5].group_by {|e| e%2},res,"array")

	res = {}.p2group_by {|e| e[1]%2}
	printerror(__method__,{}.group_by {|e| e[1]%2},res,"empty hash")
	res = {'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5}.to_h.p2group_by {|e| e[1]%2}
	printerror(__method__,{'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5}.group_by {|e| e[1]%2},res,"hash")
end

test_p2group_by

def test_p2inject
	p "#{__method__}"
	res = [].p2inject(0) {|res, e| res * e  }
	printerror(__method__,[].inject(0) {|res, e| res * e  },res,"empty array")
	res = [1,2,3,4].p2inject(0) {|res, e| res * e  }
	printerror(__method__,0,res,"array")

	sum = {}.p2inject(0) {|res, e| res + e[1]  }
	printerror(__method__,{}.inject(0) {|res, e| res + e[1]  },sum,"empty hash")
	sum = {'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5}.p2inject(0) {|e, res| res + e[1]}
	puts sum
	printerror(__method__,15,sum,"hash")
end

# test_p2inject

def test_p2para0inject 
	p "#{__method__}"
	res = [].p2para0inject {|res, e| res * e  }
	printerror(__method__,[].inject {|res, e| res * e  },res,"empty array")
	res = [1,2,3,4].p2para0inject {|res, e| res * e  }
	printerror(__method__,24,res,"array")

	res = {}.p2para0inject {|res, e| res * e[1]  }
	printerror(__method__,{}.inject {|res, e| res = res * e[1]  },res,"empty hash")
	res = {1=>1,2=>2,3=>3,4=>4}.p2para0inject {|res, e| res = res * e[1]  }
	printerror(__method__,{1=>1,2=>2,3=>3,4=>4}.inject {|res, e| res = res * e[1]  },res,"hash")
end

test_p2para0inject

def test_p2minmax
	p "#{__method__}"
	res = [].p2minmax {|e1,e2| -e1 <=> -e2}
	printerror(__method__,[].minmax {|e1,e2| -e1 <=> -e2},res,"empty array")
	res = [1,4,0,-10,7].p2minmax {|e1,e2| -e1 <=> -e2}
	printerror(__method__,[7,-10],res,"array")

	res = {}.p2minmax_by {|e1,e2| -e1[1] <=> -e2[1]}
	printerror(__method__,{}.minmax {|e1,e2| -e1[1] <=> -e2[1] },res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2minmax {|e1,e2| -e1[1] <=> -e2[1] }
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.minmax {|e1,e2| -e1[1] <=> -e2[1]},res,"hash")
end

test_p2minmax

def test_p2minmax_by
	p "#{__method__}"
	res = [].p2minmax_by {|e| e**2}
	printerror(__method__,[].minmax_by {|e| e**2},res,"empty array")
	res = [1,4,0,-10,7].p2minmax_by {|e| e**2}
	printerror(__method__,[0,-10],res,"array")

	res = {}.p2minmax_by {|e| e[1]**2}
	printerror(__method__,{}.minmax_by {|e| e[1]**2},res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2minmax_by {|e| e[1]**2}
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.minmax_by {|e| e[1]**2},res,"hash")

end

# test_p2minmax_by

def test_p2partition
	p "#{__method__}" 
	res = [].p2partition {|e| e <= 0}
	printerror(__method__,[].partition {|e| e <= 0},res,"empty array")
	res = [1,4,0,-10,-7,3].p2partition {|e| e<=0}
	printerror(__method__,[[0,-10,-7],[1,4,3]],res,"array")

	res = {}.p2partition {|e| e[1] <= 0}
	printerror(__method__,[].partition {|e| e[1] <= 0},res,"empty hash")
	res = {1=>1,2=>4,3=>0,4=>-10,5=>-7,6=>3}.p2partition {|e| e[1]<=0}
	printerror(__method__,{1=>1,2=>4,3=>0,4=>-10,5=>-7,6=>3}.partition {|e| e[1]<=0},res,"hash")
end

test_p2partition

def test_p2reject 
	p "#{__method__}"
	res = [].p2reject {|e| e%2==1}
	printerror(__method__,[].reject {|e| e%2==1},res,"empty array")
	res = [1,2,3,4,5].p2reject {|e| e%2==1}
	printerror(__method__,[2,4],res,"array")

	# res = {}.p2reject {|e| e[1]%2==1}
	# printerror(__method__,{}.reject {|e| e[1]%2==1},res,"empty hash")
	# res = {1=>1,2=>2,3=>3,4=>4,5=>5}.p2reject {|e| e[1]%2==1}
	# printerror(__method__,{1=>1,2=>2,3=>3,4=>4,5=>5}.reject {|e| e[1]%2==1},res,"hash")
end

test_p2reject

def test_p2take
	p "#{__method__}"
	res = [].p2take(3)
	printerror(__method__,[].take(3),res,"empty array")
	res = ["b","e","i","j","i","n","g"].p2take(3)
	printerror(__method__,["b","e","i"],res,"array")

	res = {}.p2take(3)
	printerror(__method__,{}.take(3),res,"empty hash")
	res = {1=>"b",2=>"e",3=>"i",4=>"j",5=>"i",6=>"n",7=>"g"}.p2take(3).p2take(3)
	printerror(__method__,{1=>"b",2=>"e",3=>"i",4=>"j",5=>"i",6=>"n",7=>"g"}.take(3),res,"hash")

end

test_p2take

def test_p2take_while 
	p "#{__method__}"
	res = [].p2take_while {|e| e != 'j'}
	printerror(__method__,[].take_while {|e| e != 'j'},res,"empty array")
	res = ["b","e","i","j","i","n","g"].p2take_while {|e| e != 'j'}
	printerror(__method__,["b","e","i"],res,"array")

	res = {}.p2take_while {|e| e[1] != 'j'}
	printerror(__method__,{}.take_while {|e| e[1] != 'j'},res,"empty hash")
	res = {1=>"b",2=>"e",3=>"i",4=>"j",5=>"i",6=>"n",7=>"g"}.p2take_while {|e| e[1] != 'j'}
	printerror(__method__,{1=>"b",2=>"e",3=>"i",4=>"j",5=>"i",6=>"n",7=>"g"}.take_while {|e| e[1] != 'j'},res,"hash")
end

test_p2take_while

def test_p2to_a
	p "#{__method__}"
	res = {}.p2to_a
	printerror(__method__,{}.to_a,res,"empty hash parseto array")
	res = (1..3).p2to_a.inject{|res, e| res + e}
	printerror(__method__,6,res,"round parseto array")

end

test_p2to_a

def test_p2to_h
	p "#{__method__}"
	res = [].p2to_h
	printerror(__method__,{},res,"empty array parseto hash")
	res = [["a",1],["b",2],["c",3],["a",4],["e",5]].p2to_h.values.inject {|sum,e | sum = sum + e}
	printerror(__method__,14,res,"array parseto hash")
	# puts %i[hello world].each_with_index.to_h
end

# test_p2to_h





def test_p2min
	p "#{__method__}"
	res = [].p2min {|e1,e2| -e1 <=> -e2}
	printerror(__method__,[].min { |e1, e2| -e1 <=> -e2  },res,"empty array")
	res = [1,2,-4,8].p2min {|e1,e2| -e1 <=> -e2}
	printerror(__method__,8,res,"array")

	res = {}.p2min {|e1,e2| -e1[1] <=> -e2[1]}
	printerror(__method__,{}.min {|e1,e2| -e1[1] <=> -e2[1] },res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2min {|e1,e2| -e1[1] <=> -e2[1] }
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.min {|e1,e2| -e1[1] <=> -e2[1]},res,"hash")

end

test_p2min

def test_p2min_by
	p "#{__method__}"
	res = [].p2min_by {|e| e**2 }
	printerror(__method__,[].min_by { |e| e**2 },res,"empty array")
	res = [1,2,-4,8].p2min_by {|e| e**2}
	printerror(__method__,1,res,"array")

	res = {}.p2min_by {|e| e[1]**2}
	printerror(__method__,{}.min_by {|e| e[1]**2},res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2min_by {|e| e[1]**2}
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.min_by {|e| e[1]**2},res,"hash")
end

test_p2min_by

def test_p2max
	p "#{__method__}"
	res = [].p2max {|e1,e2| -e1 <=> -e2}
	printerror(__method__,[].max {|e1,e2| -e1 <=> -e2},res,"empty array")
	res = [1,2,-4,8].p2max {|e1,e2| -e1 <=> -e2}
	printerror(__method__,-4,res,"array")

	res = {}.p2max {|e| e[1]**2}
	printerror(__method__,{}.max {|e| e[1]**2},res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2max {|e| e[1]**2}
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.max {|e| e[1]**2},res,"hash")
end

test_p2max

def test_p2max_by
	p "#{__method__}"
	res = [].p2max_by {|e| e**2}
	printerror(__method__,[].max_by {|e| e**2},res,"empty array")
	res = [1,2,-4,8].p2max_by {|e| e**2}
	printerror(__method__,8,res,"array")

	res = {}.p2max_by {|e| e[1]**2}
	printerror(__method__,{}.max_by {|e| e[1]**2},res,"empty hash")
	res = {'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.p2max_by {|e| e[1]**2}
	printerror(__method__,{'a'=>1,'b'=>4,'c'=>0,'d'=>-10,'e'=>7}.max_by {|e| e[1]**2},res,"hash")
end

test_p2max_by

def test_p2none?
	p "#{__method__}"
	res = [].p2none?
	printerror(__method__, [].none?,res,"empty array without block")
	res = [true,1,4,8].p2none?
	printerror(__method__,false,res,"array without block")
	res = [1,2,-4,8].p2none? {|e| e > 10}
	printerror(__method__,true,res,"array")

	res = {}.p2none?{|e| e[1] > 10}
	printerror(__method__, {}.none?{|e| e[1] > 10},res,"empty hash")
	res = {1=>1,2=>2,3=>-4,4=>8}.p2none? {|e| e[1] > 10}
	printerror(__method__,true,res,"hash")


	
end

test_p2none?

def test_p2one?
	p "#{__method__}"
	res = [true,true,1,4,8].p2one?
	printerror(__method__,false,res,"array without block and have 2+ true value")
	res = [1,4,nil,8].p2one?
	printerror(__method__,false,res,"array without block and have 0 true value")
	res = ["Object-C","Java","Python","HTML","Ruby"].p2one? {|e| e.length<=4}
	printerror(__method__,false,res,"array with more than one element that let the block return true")
	res = ["Object-C","Java","Python","HTML","Ruby","Swift"].p2one? {|e| e.length == 6}
	printerror(__method__,true,res,"array with only one element that let the block return true")

	res = {1=>true,2=>true,3=>1,4=>4,5=>8}.p2one?
	printerror(__method__,false,res,"hash without block and have 2+ true value")
	res = {1=>1,2=>4,3=>nil,4=>8}.p2one?
	printerror(__method__,false,res,"hash without block and have 0 true value")
	res = {1=>"Object-C",2=>"Java",3=>"Python",4=>"HTML",5=>"Ruby"}.p2one? {|e| e[1].length<=4}
	printerror(__method__,false,res,"hash with more than one element that let the block return true")
	res = {1=>"Object-C",2=>"Java",3=>"Python",4=>"HTML",5=>"Ruby",6=>"Swift"}.p2one? {|e| e[1].length == 6}
	printerror(__method__,true,res,"hash with only one element that let the block return true")
	
end

test_p2one?
